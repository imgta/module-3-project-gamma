### 20Jun23

1. Experimented with OCR (optical character recognition) libs for receipt scanning
   -OpenCV lib for image grayscale conversion
   -OCR.space API for text parsing
   -regex lib for cleanup, formatting
   (Still not reliable--missing characters, character mismatches)

### 21Jun23

1. OCR Feature: somewhat solved receipt text parsing with Google's Cloud Vision API
   -established code for extracting date, costs, subtotal, taxrate, total
   -also checks if the sum of all charges is equal to the total amount
2. GPS Metadata
   -Pillow lib for exif tag extraction from images
   -code to convert latitude/longitude to workable degree coordinates

### 23Jun23

1. Started building out site templates on Figma.

### 26Jun23

1. Created User Sign Up Issue in GitLab

### 27Jun23

1. Edits to docker-compose.yaml:
   -renamed PostgreSQL database to backtab_db
   -added WAITs to api service environ
   -changed postgres-db port back to "15432:5432" instead of "5432:5432":
   --in the case of developers already having postgres installed and running on their workstations, exposing the host port to 15432 instead of the default 5432 (hopefully) mitigates conflicts when attempting to run an additional postgres container
2. With entire group: created/initiated users_table in migrations, dropped big_dummy table
3. Installed/froze some pip libraries, started building out User models
4. Created shell scripts to use git alias commands to run pushes and pulls to/from main branch

### 28Jun23

1. Created users query class models, authenticator.py
2. Group work on the user create function led by Dimitri

### 29Jun23

1. Built out authentication and fetch functions for create user, show user, show users
2. Re-org all model classes into models/users.py

### 30Jun23

1. Finally fixed the authentication TypeError for date objects:
   -needed a datetime encoder (ty Dimitri) for authenticator
   -changes to get_account_data_for_cookie function (json loads/dumps)

### 10Jul23

1. Started frontend authentication

### 11Jul23

1. Frontend Auth: username and password regex patterns, registration form

### 12Jul23

1. Frontend Auth: experimenting with axios
   -completed JS + HTML for registration form

### 13Jul23

1. Fixed frontend auth (404 Token error)
2. Removed "created" date column from migration table

### 14Jul23

1. Implemented main page indicator for logged in/logged out
2. Used Dimitri's date formatting code to allow for input type="date" to be accepted in registration form
3. Created navigation bar with Dimitri, implemented logout button, added logo

### 15Jul23

1. Created groups and groups_invite table with models, queries, routers, foreign key, etc.
   -added function to auto-populate groups creator column with current user that created group
2. Fixed receipts and receipt_items tables:
   -added two new columns: image_url, settled (boolean)
   -added foreign key reference
3. Added ON DELETE CASCADE constraints to foreign key/references columns
4. Expanded gps.py code to allow extrapolation of street address (Google Geocode API) and venue name (Google Places API) from EXIF-data-derived lat/long coordinates
   -still need to test for accurate venue names (experimented with radius and type=point_of_interest parameters)
5. Created receipt_ocr table for subsequent preliminary data via OCR data parsing

### 16Jul23

1. Expanded/fixed gps.py code:
   -installed pillow_heif library plugin to enable .HEIC image processing
   -established/fine-tuned Places API parameters to isolate bill-incurring places of interest ("type" param bypass)
2. Places API "radius" parameter is disallowed if using "rankby=distance" parameter concurrently
   -"rankby+radius" constraint bypass: code to sort nearby venues by distance via calculated coordinate deltas

### 17Jul23

1. Added auto-focus useEffects on Login and SignUp pages (auto-selects first input field when navigating to page)
2. Added homepage redirect when logging out
3. Amazon S3 Bucket
   -created s3 bucket for receipt images
   -configured CORS settings
   -set up IAM user with PUT, GET, DELETE permissions

### 18Jul23

1. Fixed foreign key references in tables to use serialized ids instead of names
   -unnecessary complications when trying to route data in SQL tables vs. python code
2. Created/expanded expenses table to track who ordered and who paid
3. Implemented enum type "status" for group_invites ('ACCEPTED', 'INVITED', 'REJECTED')

### 19Jul23

1. Created models and functions to update users' fullname, email, or password:
   -update_user function re-hashes password, only allows users to update their own accounts
2. Finished group functions, created group_invite functions
3. Started SMTP functionality with Redmail and Gmail to send invite links to user emails
4. Organized HTTP requests in SwaggerUI by category (tags=["arg"])

### 20Jul23

1. Complete overhaul of users, groups, group_invites pydantic models, queries and router functions.
2. Created **root**: List[] models for more intuitive value-type validations in functions
3. Condensed most cursor SQL executes in queries to use SELECT\* to leverage the more versatile .fetchall() method
4. Refactored queries functions to neatly package .fetchall() data into a workable dictionary by iterating the .description attribute for each column
