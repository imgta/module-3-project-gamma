### June 26, 2023:
Created issues on gitlab for user sign in/sign up

### June 27, 2023:
Created tables, practice migrations.
Added pg-admin to docker yaml

### June 28, 2023:
Discuss backend functionalities and started creating endpoints for
users. Group wanted to use axios for auth and fetch.

### June 29, 2023:
Start on backend authentication using jwt-down

### June 30, 2023:
Troubleshooting backend authentication w/ group

### July 10, 2023:
Frontend authentication started, group wanted to go with axios? for auth and fetch.
Spent all day learning about it and trying to apply it

### July 11, 2023:
Errors w/ axios auth, unsure how to connect frontend with backend auth since we are using 2
different auths

### July 12, 2023:
Created login form on frontend, still researching axios auth

### July 13, 2023:
Group decides to scrap axios and use the provided frontend auth.
Restart w/ reading documentation for login feature so I can apply it
to the login form

### July 14, 2023:
Work on login form functionality

### July 15, 2023:
Start frontend work, receipt form and add items form

### July 16, 2023:
Continue frontend forms work

### July 17, 2023:
Create unit test, continue form work. Need to figure out the routing to
add receipts and items once backend receipts and items are complete

### July 18, 2023:
Unit testing, trouble shooting those

### July 19, 2023:
Having issues w/ gitlab pull and docker all day. Updating other unit tests to
match changed code

### July 20, 2023:
