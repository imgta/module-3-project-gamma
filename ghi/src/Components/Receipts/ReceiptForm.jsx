import useToken from "@galvanize-inc/jwtdown-for-react";
import React, { useState, useEffect } from "react";

export default function ReceiptForm() {
  const [venue, setVenue] = useState("");
  const [address, setAddress] = useState("");
  const [date, setDate] = useState("");
  const [imgUrl, setImgUrl] = useState("");
  const [total, setTotalCost] = useState(0);
  const [receiptName, setReceiptName] = useState("");
  const [settled, setSettled] = useState(false);
  const [groups, setGroups] = useState([]);
  const [groupId, setGroupId] = useState(0);
  const { token } = useToken();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.venue_name = venue;
    data.receipt_name = receiptName;
    data.address = address;
    data.total_cost = total;
    data.assigned_group = groupId;
    data.image_url = imgUrl;
    data.transaction_date = date;
    data.settled = settled;
    console.log(data);

    const receiptsUrl = `${process.env.REACT_APP_API_HOST}/receipts/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };

    const response = await fetch(receiptsUrl, fetchConfig);
    if (response.ok) {
      setReceiptName("");
      setVenue("");
      setAddress("");
      setDate("");
      setImgUrl("");
      setTotalCost(0);
      setSettled(false);
    }
  };

  const fetchData = async () => {
    const headers = { Authorization: `Bearer ${token}` };
    const url = `${process.env.REACT_APP_API_HOST}/groups/me`;

    const response = await fetch(url, { headers, credentials: "include" });

    if (response.ok) {
      const data = await response.json();
      setGroups(data);
    }
  };

  useEffect(() => {
    if (token) {
      fetchData();
    }
  }, [token]);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Receipt</h1>
          <form onSubmit={handleSubmit}>
            <label>
              Receipt Name:
              <input
                type="text"
                value={receiptName}
                onChange={(e) => setReceiptName(e.target.value)}
                className="input input-bordered form-control"
              />
            </label>

            <label>
              Venue:
              <input
                name="Venue"
                type="text"
                value={venue}
                onChange={(e) => setVenue(e.target.value)}
                className="input input-bordered form-control"
              />
            </label>

            <label className="label">Date:</label>
            <input
              type="date"
              value={date}
              onChange={(e) => setDate(e.target.value)}
              className="input-bordered form-control input"
            />

            <label className="label">Upload image:</label>
            <input
              type="file"
              value={imgUrl}
              onChange={(e) => setImgUrl(e.target.value)}
              className="file-input file-input-bordered"
            />

            <label>
              Total Cost:
              <input
                type="number"
                value={total}
                onChange={(e) => setTotalCost(parseFloat(e.target.value))}
                className="input input-bordered form-control"
              />
            </label>
            <br />

            <div className="mb-3">
              <select
                value={groupId}
                onChange={(e) => setGroupId(Number(e.target.value))}
                className="form-select"
              >
                <option>Choose a Group</option>
                {groups?.map((group) => {
                  return (
                    <option key={group.group_id} value={group.group_id}>
                      {group.name}
                    </option>
                  );
                })}
              </select>
            </div>

            <label>Settled?</label>
            <div className="mb-3">
              <select
                title="Settled?"
                value={settled}
                onChange={(e) => setSettled(e.target.value)}
                className="form-select"
              >
                <option value={true}>Yes</option>
                <option value={false}>No</option>
              </select>
            </div>
            <br />
            <button
              type="submit"
              className="btn btn-xs btn-primary sm:btn-sm md:btn-md lg:btn-lg"
            >
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
