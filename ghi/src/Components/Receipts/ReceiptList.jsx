import React from 'react';
import { useEffect, useState } from 'react';
import useToken from '@galvanize-inc/jwtdown-for-react';

function ReceiptList() {
	const [receipts, setReceipts] = useState([]);
	const { token } = useToken();

	const fetchData = async() => {
		try {
			const receiptListUrl = `${process.env.REACT_APP_API_HOST}/receipts`;
			const response = await fetch(receiptListUrl, {
				method:"GET",
				headers: {
					Authorization: `Bearer ${token}`,
				},
			});

			if (response.ok) {
				const data = await response.json();
				setReceipts(data)
				console.log(data)
			} else {
				console.error("Failed to fetch receipts:", response.statusText);
			}
		} catch (error) {
			console.error("Error fetching receipts:", error);
		}
	};

	useEffect(() => {
		if (token) {
		fetchData();
		}
	}, [token]);

	return (
		<div className="container">
			<h2>Your Receipts</h2>
			<table className="table">
				<thead>
					<tr>
						<th>Receipt Name</th>
						<th>Venue</th>
						<th>Total Cost</th>
						<th>Transacted Date</th>
						<th>All Settled?</th>

						{/* figure out how to get the assigned group */}
						<th>Assigned Group</th>
					</tr>
				</thead>
				<tbody>
					{receipts?.map(receipt => {
						return (
							<tr key={receipt.id}>
								<td>{ receipt.receipt_name }</td>
								<td>{ receipt.venue_name }</td>
								<td> ${ receipt.total_cost }</td>
								<td>{ receipt.transaction_date }</td>
								<td>{ receipt.settled ? "Settled!":"Not yet" }</td>
								<td> figue this out</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}

export default ReceiptList
