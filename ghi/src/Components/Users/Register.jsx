import React, { useRef, useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

export default function RegisterForm() {
  const formRef = useRef(null);
  const [fullname, setFullname] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [birthdate, setBirthdate] = useState("");
  const [formSubmitted, setFormSubmitted] = useState(false);
  const [validUsername, setValidUsername] = useState(true);
  const [validPassword, setValidPassword] = useState(true);
  const [validConfirmPassword, setValidConfirmPassword] = useState(true);

  const { register } = useToken();
  const navigate = useNavigate();
  const [errorMsg, setErrorMsg] = useState([]);
  const userRef = useRef();

  const [usernameFocus, setUsernameFocus] = useState(false);
  const [passwordFocus, setPasswordFocus] = useState(false);

  const resetForm = () => {
    setFullname("");
    setUsername("");
    setEmail("");
    setPassword("");
    setConfirmPassword("");
    setBirthdate("");
    setFormSubmitted("");
    formRef.current.reset();
  };

  useEffect(() => {
    userRef.current.focus();
  }, []);

  useEffect(() => {
    if (formSubmitted) {
      resetForm();
    }
  }, [formSubmitted]);

  useEffect(() => {
    const userREGEX = /^[a-z0-9][a-z0-9_]{3,19}$/;
    const isValid = userREGEX.test(username);
    setValidUsername(isValid);
  }, [username]);

  useEffect(() => {
    const isValid = password.length >= 8 && password.length <= 24;
    setValidPassword(isValid);
  }, [password]);

  useEffect(() => {
    const isValid = password === confirmPassword;
    setValidConfirmPassword(isValid);
  }, [password, confirmPassword]);

  useEffect(() => {
    setErrorMsg("");
  }, [username, password, confirmPassword]);

  const handleRegister = (e) => {
    e.preventDefault();
    const errorMsg = [];
    if (!validUsername) {
      errorMsg.push("Invalid username.");
    }
    if (!validPassword) {
      errorMsg.push("Invalid password.");
    }
    if (!validConfirmPassword) {
      errorMsg.push("Password mismatch.");
    }
    if (errorMsg.length > 0) {
      setErrorMsg(errorMsg.join(" "));
      return;
    }
    const accountData = {
      fullname: fullname,
      email: email,
      username: username,
      password: password,
      birthdate: birthdate,
    };
    register(accountData, `${process.env.REACT_APP_API_HOST}/users`);
    e.target.reset();
    navigate("/");
  };

  return (
    <>
      <div className="bg-base-300">
        <h1 className="text-primary-focus text-5xl font-bold text-center pt-2">
          Join the party!
        </h1>
        <p className="pb-4 pt-1 text-secondary text-center">
          Plan. Play. Connect.
        </p>
      </div>
      <div className="hero bg-base-300">
        <div className="hero-content flex-col lg:flex-row-reverse">
          <div className="lg:text-left text-base-content"></div>

          <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-200">
            <form onSubmit={handleRegister}>
              <div className="card-body bg-base-200 py-1">
                <div className="bg-base-200">
                  <label className="label text-base-content">Full Name</label>
                  <input
                    type="text"
                    id="fullname"
                    ref={userRef}
                    value={fullname}
                    name="fullname"
                    className="form-control input input-bordered"
                    onChange={(e) => setFullname(e.target.value)}
                    required
                  />
                </div>
                <div className="bg-base-200">
                  <label className="label text-base-content mt-0">Email</label>
                  <input
                    type="email"
                    id="email"
                    name="email"
                    value={email}
                    className="form-control input input-bordered"
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </div>
                <div className="bg-base-200">
                  <label className="label text-base-content mt-0">
                    Username
                  </label>
                  <input
                    type="text"
                    id="username"
                    name="username"
                    value={username}
                    className="form-control input input-bordered"
                    onChange={(e) => setUsername(e.target.value)}
                    required
                    aria-invalid={validUsername ? "false" : "true"}
                    aria-describedby="uidnote"
                    onFocus={() => setUsernameFocus(true)}
                    onBlur={() => setUsernameFocus(false)}
                  />
                  <p
                    id="uidnote"
                    className={
                      usernameFocus && username && !validUsername
                        ? "instructions"
                        : "offscreen"
                    }
                  >
                    4 to 20 characters. <br />
                    Must begin with a letter. <br />
                    Letters, numbers, underscores, allowed.
                  </p>
                </div>

                <div className="bg-base-200">
                  <label className="label text-base-content mt-0">
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    name="password"
                    value={password}
                    className="form-control input input-bordered"
                    onChange={(e) => setPassword(e.target.value)}
                    autoComplete="off"
                    required
                    aria-invalid={validPassword ? "false" : "true"}
                    aria-describedby="pwdnote"
                    onFocus={() => setPasswordFocus(true)}
                    onBlur={() => setPasswordFocus(false)}
                  />
                  <p
                    id="pwdnote"
                    className={
                      passwordFocus && !validPassword
                        ? "instructions"
                        : "offscreen"
                    }
                  >
                    Must be 8 to 24 characters.
                  </p>
                </div>

                <div className="bg-base-200">
                  <label className="label text-base-content mt-0">
                    Confirm Password
                  </label>
                  <input
                    type="password"
                    autoComplete="off"
                    id="confirmPassword"
                    name="confirmPassword"
                    value={confirmPassword}
                    className="form-control input input-bordered"
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    required
                  />
                  {!validConfirmPassword && (
                    <p className="error-message">Passwords do not match.</p>
                  )}
                  {errorMsg && <p className="error-message">{errorMsg}</p>}
                </div>

                <div className="bg-base-200">
                  <label className="label text-base-content mt-0">
                    Birthdate
                  </label>
                  <input
                    type="date"
                    id="birthdate"
                    name="birthdate"
                    value={birthdate}
                    className="form-control input-bordered input"
                    onChange={(e) => {
                      const enterDate = e.target.value;
                      setBirthdate(enterDate);
                    }}
                    required
                  />
                </div>

                <div className="mt-4 bg-base-200 flex justify-center">
                  <button
                    className="btn btn-primary btn-wide"
                    type="submit"
                    value="Register"
                  >
                    Register
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>

    // <>
    //   <h2 className="card-header">Join the party!</h2>
    //   <form onSubmit={handleRegister} id="registration-form">
    //     <label htmlFor="fullname">Full Name: </label>
    //     <input
    //       type="text"
    //       id="fullname"
    //       name="fullname"
    //       ref={userRef}
    //       value={fullname}
    //       autoComplete="off"
    //       onChange={(e) => setFullname(e.target.value)}
    //       required
    //     />
    //     <label htmlFor="email">Email: </label>
    //     <input
    //       type="email"
    //       id="email"
    //       name="email"
    //       value={email}
    //       autoComplete="off"
    //       onChange={(e) => setEmail(e.target.value)}
    //       required
    //     />
    //     <label htmlFor="username">Username: </label>
    //     <input
    //       type="text"
    //       id="username"
    //       name="username"
    //       autoComplete="off"
    //       onChange={(e) => setUsername(e.target.value)}
    //       required
    //       aria-invalid={validUsername ? "false" : "true"}
    //       aria-describedby="uidnote"
    //       onFocus={() => setUsernameFocus(true)}
    //       onBlur={() => setUsernameFocus(false)}
    //     />
    //     <p
    //       id="uidnote"
    //       className={
    //         usernameFocus && username && !validUsername
    //           ? "instructions"
    //           : "offscreen"
    //       }
    //     >
    //       4 to 20 characters. <br />
    //       Must begin with a letter. <br />
    //       Letters, numbers, underscores, allowed.
    //     </p>
    //     <label>Password: </label>
    //     <input
    //       type="password"
    //       id="password"
    //       name="password"
    //       onChange={(e) => setPassword(e.target.value)}
    //       autoComplete="off"
    //       required
    //       aria-invalid={validPassword ? "false" : "true"}
    //       aria-describedby="pwdnote"
    //       onFocus={() => setPasswordFocus(true)}
    //       onBlur={() => setPasswordFocus(false)}
    //     />
    //     <p
    //       id="pwdnote"
    //       className={
    //         passwordFocus && !validPassword ? "instructions" : "offscreen"
    //       }
    //     >
    //       Must be 8 to 24 characters.
    //     </p>
    //     <label htmlFor="confirmPassword"> Confirm Password: </label>
    //     <input
    //       type="password"
    //       autoComplete="off"
    //       id="confirmPassword"
    //       name="confirmPassword"
    //       value={confirmPassword}
    //       onChange={(e) => setConfirmPassword(e.target.value)}
    //       required
    //     />
    //     {!validConfirmPassword && (
    //       <p className="error-message">Passwords do not match.</p>
    //     )}
    //     {errorMsg && <p className="error-message">{errorMsg}</p>}
    // <label htmlFor="birthdate">Birthdate: </label>
    // <input
    //   type="date"
    //   id="birthdate"
    //   name="birthdate"
    //   value={birthdate}
    //   onChange={(e) => {
    //     const enterDate = e.target.value;
    //     const formattedDate = formatDate(enterDate);
    //     setBirthdate(formattedDate);
    //   }}
    //   required
    // />
    //     <br />{" "}
    //     <button
    //       type="submit"
    //       value="Register"
    //       className="btn btn-primary"
    //       // disabled
    //     >
    //       Register
    //     </button>
    //   </form>
    // </>
  );
}
