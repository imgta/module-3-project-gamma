import React, { useState, useEffect, useCallback } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Link } from "react-router-dom";
import axios from "axios";

export default function UserDashboard() {
  const [username, setUsername] = useState("");
  const [userInitials, setUserInitials] = useState("");
  const { logout, token } = useToken();

  const getUsername = useCallback(async () => {
    try {
      const res = await axios.get(`${process.env.REACT_APP_API_HOST}/user/me`, {
        headers: { Authorization: `Bearer ${token}` },
      });
      setUsername(res.data.username);
      const firstlast = res.data.fullname.split(" ");
      const initials = firstlast
        .map((name) => name.charAt(0).toUpperCase())
        .join("");
      setUserInitials(initials);
    } catch (err) {
      console.log(err);
    }
  }, [token]);

  useEffect(() => {
    if (token) {
      getUsername();
    }
  }, [token, getUsername]);

  return (
    <div className="bg-base-100 text-content flex">
      <ul className="menu bg-base-100 rounded-box pt-3">
        <div className="avatar placeholder bg-base-100 cursor-default self-center mb-2">
          <div className="bg-base-content text-base-100 text-sm rounded-full w-8">
            {userInitials}
          </div>
        </div>
        <li>
          <Link to="/">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              className="fill-base-content"
              viewBox="0 0 256 256"
            >
              <path d="M240,204H228V115.55a20.07,20.07,0,0,0-6.44-14.7L141.61,25.38l-.16-.15a19.93,19.93,0,0,0-26.91,0l-.17.15L34.44,100.85A20.07,20.07,0,0,0,28,115.55V204H16a12,12,0,0,0,0,24H240a12,12,0,0,0,0-24ZM52,117.28l76-71.75,76,71.75V204H164V160a20,20,0,0,0-20-20H112a20,20,0,0,0-20,20v44H52ZM140,204H116V164h24Z"></path>
            </svg>
            <span className="text-base-content">Home</span>
          </Link>
        </li>
        <div className="dropdown dropdown-hover">
          <label tabIndex={0} className="btn m-2">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="18"
              height="18"
              fill="#000000"
              className="fill-base-content"
              viewBox="0 0 256 256"
            >
              <path d="M27.2,126.4a8,8,0,0,0,11.2-1.6,52,52,0,0,1,83.2,0,8,8,0,0,0,11.2,1.59,7.73,7.73,0,0,0,1.59-1.59h0a52,52,0,0,1,83.2,0,8,8,0,0,0,12.8-9.61A67.85,67.85,0,0,0,203,93.51a40,40,0,1,0-53.94,0,67.27,67.27,0,0,0-21,14.31,67.27,67.27,0,0,0-21-14.31,40,40,0,1,0-53.94,0A67.88,67.88,0,0,0,25.6,115.2,8,8,0,0,0,27.2,126.4ZM176,40a24,24,0,1,1-24,24A24,24,0,0,1,176,40ZM80,40A24,24,0,1,1,56,64,24,24,0,0,1,80,40ZM203,197.51a40,40,0,1,0-53.94,0,67.27,67.27,0,0,0-21,14.31,67.27,67.27,0,0,0-21-14.31,40,40,0,1,0-53.94,0A67.88,67.88,0,0,0,25.6,219.2a8,8,0,1,0,12.8,9.6,52,52,0,0,1,83.2,0,8,8,0,0,0,11.2,1.59,7.73,7.73,0,0,0,1.59-1.59h0a52,52,0,0,1,83.2,0,8,8,0,0,0,12.8-9.61A67.85,67.85,0,0,0,203,197.51ZM80,144a24,24,0,1,1-24,24A24,24,0,0,1,80,144Zm96,0a24,24,0,1,1-24,24A24,24,0,0,1,176,144Z"></path>
            </svg>

            <span className="text-base-content">Groups</span>
          </label>
          <ul
            tabIndex={0}
            className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
          >
            <li>
              <Link to="/create-group">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="18"
                  height="18"
                  fill="#000000"
                  className="fill-base-content"
                  viewBox="0 0 256 256"
                >
                  <path d="M168,56a8,8,0,0,1,8-8h16V32a8,8,0,0,1,16,0V48h16a8,8,0,0,1,0,16H208V80a8,8,0,0,1-16,0V64H176A8,8,0,0,1,168,56Zm62.56,54.68a103.92,103.92,0,1,1-85.24-85.24,8,8,0,0,1-2.64,15.78A88.07,88.07,0,0,0,40,128a87.62,87.62,0,0,0,22.24,58.41A79.66,79.66,0,0,1,98.3,157.66a48,48,0,1,1,59.4,0,79.66,79.66,0,0,1,36.06,28.75A87.62,87.62,0,0,0,216,128a88.85,88.85,0,0,0-1.22-14.68,8,8,0,1,1,15.78-2.64ZM128,152a32,32,0,1,0-32-32A32,32,0,0,0,128,152Zm0,64a87.57,87.57,0,0,0,53.92-18.5,64,64,0,0,0-107.84,0A87.57,87.57,0,0,0,128,216Z"></path>
                </svg>
                <span className="text-base-content">Create Group</span>
              </Link>
            </li>
            <li>
              <Link to="/group-list">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  className="fill-base-content"
                  viewBox="0 0 256 256"
                >
                  <path d="M125.18,156.94a64,64,0,1,0-82.36,0,100.23,100.23,0,0,0-39.49,32,12,12,0,0,0,19.35,14.2,76,76,0,0,1,122.64,0,12,12,0,0,0,19.36-14.2A100.33,100.33,0,0,0,125.18,156.94ZM44,108a40,40,0,1,1,40,40A40,40,0,0,1,44,108Zm206.1,97.67a12,12,0,0,1-16.78-2.57A76.31,76.31,0,0,0,172,172a12,12,0,0,1,0-24,40,40,0,1,0-14.85-77.16,12,12,0,1,1-8.92-22.28,64,64,0,0,1,65,108.38,100.23,100.23,0,0,1,39.49,32A12,12,0,0,1,250.1,205.67Z"></path>
                </svg>
                <span className="text-base-content">Your Groups</span>
              </Link>
            </li>
          </ul>
        </div>
        <div className="dropdown dropdown-hover">
          <label tabIndex={0} className="btn m-2">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="18"
              height="18"
              fill="#000000"
              viewBox="0 0 256 256"
              className="fill-base-content"
            >
              <path d="M72,104a8,8,0,0,1,8-8h96a8,8,0,0,1,0,16H80A8,8,0,0,1,72,104Zm8,40h96a8,8,0,0,0,0-16H80a8,8,0,0,0,0,16ZM232,56V208a8,8,0,0,1-11.58,7.15L192,200.94l-28.42,14.21a8,8,0,0,1-7.16,0L128,200.94,99.58,215.15a8,8,0,0,1-7.16,0L64,200.94,35.58,215.15A8,8,0,0,1,24,208V56A16,16,0,0,1,40,40H216A16,16,0,0,1,232,56Zm-16,0H40V195.06l20.42-10.22a8,8,0,0,1,7.16,0L96,199.06l28.42-14.22a8,8,0,0,1,7.16,0L160,199.06l28.42-14.22a8,8,0,0,1,7.16,0L216,195.06Z"></path>
            </svg>
            <span className="text-base-content">Receipts</span>
          </label>
          <ul
            tabIndex={0}
            className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
          >
            <li>
              <Link to="/create-receipt">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="18"
                  height="18"
                  fill="#000000"
                  viewBox="0 0 256 256"
                  className="fill-base-content"
                >
                  <path d="M213.66,82.34l-56-56A8,8,0,0,0,152,24H56A16,16,0,0,0,40,40V216a16,16,0,0,0,16,16H200a16,16,0,0,0,16-16V88A8,8,0,0,0,213.66,82.34ZM160,51.31,188.69,80H160ZM200,216H56V40h88V88a8,8,0,0,0,8,8h48V216Zm-40-64a8,8,0,0,1-8,8H136v16a8,8,0,0,1-16,0V160H104a8,8,0,0,1,0-16h16V128a8,8,0,0,1,16,0v16h16A8,8,0,0,1,160,152Z"></path>
                </svg>
                <span className="text-base-content">Create Receipt</span>
              </Link>
            </li>
            <li>
              <Link to="/receipt-list">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="18"
                  height="18"
                  fill="#000000"
                  viewBox="0 0 256 256"
                  className="fill-base-content"
                >
                  <path d="M213.66,66.34l-40-40A8,8,0,0,0,168,24H88A16,16,0,0,0,72,40V56H56A16,16,0,0,0,40,72V216a16,16,0,0,0,16,16H168a16,16,0,0,0,16-16V200h16a16,16,0,0,0,16-16V72A8,8,0,0,0,213.66,66.34ZM168,216H56V72h76.69L168,107.31v84.53c0,.06,0,.11,0,.16s0,.1,0,.16V216Zm32-32H184V104a8,8,0,0,0-2.34-5.66l-40-40A8,8,0,0,0,136,56H88V40h76.69L200,75.31Zm-56-32a8,8,0,0,1-8,8H88a8,8,0,0,1,0-16h48A8,8,0,0,1,144,152Zm0,32a8,8,0,0,1-8,8H88a8,8,0,0,1,0-16h48A8,8,0,0,1,144,184Z"></path>
                </svg>
                <span className="text-base-content">Your Receipts</span>
              </Link>
            </li>
            <li>
              <Link to="/create-item">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="18"
                  height="18"
                  fill="#000000"
                  viewBox="0 0 256 256">
                    <path d="M28,64A12,12,0,0,1,40,52H216a12,12,0,0,1,0,24H40A12,12,0,0,1,28,64Zm12,76H216a12,12,0,0,0,0-24H40a12,12,0,0,0,0,24Zm104,40H40a12,12,0,0,0,0,24H144a12,12,0,0,0,0-24Zm88,0H220V168a12,12,0,0,0-24,0v12H184a12,12,0,0,0,0,24h12v12a12,12,0,0,0,24,0V204h12a12,12,0,0,0,0-24Z"></path>
                </svg>
                <span className="text-base-content">Add an item</span>
              </Link>
            </li>
            <li>
              <Link to="/item-list">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="18"
                  height="18"
                  fill="#000000"
                  viewBox="0 0 256 256">
                    <path d="M140,128v40a12,12,0,0,1-24,0V128a12,12,0,0,1,24,0ZM243.82,98.64,230,202.64A20.06,20.06,0,0,1,210.13,220H45.87A20.07,20.07,0,0,1,26,202.65l-13.86-104A20,20,0,0,1,32,76H66.55L119,16.1a12,12,0,0,1,18.06,0L189.45,76H224a20,20,0,0,1,19.81,22.64ZM98.45,76h59.11L128,42.22Zm121,24H36.57l12.8,96H206.63Zm-51.37,26.81-4,40a12,12,0,0,0,10.75,13.13c.4,0,.81.06,1.21.06a12,12,0,0,0,11.92-10.81l4-40a12,12,0,1,0-23.88-2.38Zm-80.12,0a12,12,0,0,0-23.88,2.38l4,40A12,12,0,0,0,80,180c.39,0,.8,0,1.2-.06a12,12,0,0,0,10.75-13.13Z"></path>
                </svg>
                <span className="text-base-content">Item list</span>
              </Link>
            </li>
          </ul>
        </div>
      </ul>
      <div className="w-full">
        <div className="h-[35px] m-4 rounded border-2 border-base-200 bg-base-200"></div>
        <div className="h-[400px] m-4 rounded border-2 border-base-200 bg-base-200"></div>
      </div>
    </div>

    // <div className="bg-base-100 container-fluid">
    //   <div className="row">
    //     <div className="col-auto col-sm-2 bg-dark d-flex flex-column justify-content-between min-vh-100">
    //       <div className="mt-2">
    //         <a
    //           className="text-decoration-none ms-4 d-flex align-items-center text-white d-none d-sm-inline"
    //           href="/dashboard"
    //         >
    //           <i className="bi bi-boxes"></i>
    //           <span className="f5-4"> User Dashboard</span>
    //         </a>
    //         <hr className="text-white d-none d-sm-block"></hr>
    //         <ul className="nav nav-pills flex-column mt-2 mt-sm-0" id="parentM">
    //           <li className="nav-item my-1 py-2 py-sm-0">
    //             <a
    //               className="nav-link text-white text-center text-sm-start"
    //               aria-current="page"
    //               href="/"
    //             >
    //               <i className="bi bi-house"></i>
    //               <span className="ms-2 d-none d-sm-inline">Home</span>
    //             </a>
    //           </li>
    //           <li className="nav-item my-1 py-2 py-sm-0">
    //             <a
    //               className="nav-link text-white text-center text-sm-start"
    //               data-bs-toggle="collapse"
    //               aria-current="page"
    //               href="#submenu"
    //             >
    //               <i className="bi bi-people"></i>
    //               <span className="ms-2 d-none d-sm-inline">Groups</span>
    //               <i className="bi bi-arrow-down-short ms-0 ms-sm-2"></i>
    //             </a>
    //             <ul
    //               className="nav collapse ms-2 flex-column"
    //               id="submenu"
    //               data-bs-parent="#parentM"
    //             >
    //               <li className="nav-item">
    //                 <a
    //                   className="nav-link text-white"
    //                   href="/create-group"
    //                   aria-current="page"
    //                 >
    //                   <span className="d-none d-sm-inline">Create A Group</span>
    //                 </a>
    //               </li>
    //               <li className="nav-item">
    //                 <a className="nav-link text-white" href="/list-group">
    //                   <span className="d-none d-sm-inline">Your Groups</span>
    //                 </a>
    //               </li>
    //             </ul>
    //           </li>
    //         </ul>
    //         <ul className="nav nav-pills flex-column mt-2 mt-sm-0" id="parentA">
    //           <li className="nav-item my-1 py-2 py-sm-0">
    //             <a
    //               className="nav-link text-white text-center text-sm-start"
    //               data-bs-toggle="collapse"
    //               aria-current="page"
    //               href="#receipt-submenu"
    //             >
    //               <i className="bi bi-receipt"></i>
    //               <span className="ms-2 d-none d-sm-inline">Receipts</span>
    //               <i className="bi bi-arrow-down-short ms-0 ms-sm-2"></i>
    //             </a>
    //             <ul
    //               className="nav collapse ms-2 flex-column"
    //               id="receipt-submenu"
    //               data-bs-parent="#parentA"
    //             >
    //               <li className="nav-item">
    //                 <a
    //                   className="nav-link text-white"
    //                   href="/create-group"
    //                   aria-current="page"
    //                 >
    //                   <span className="d-none d-sm-inline">
    //                     Create A Receipt
    //                   </span>
    //                 </a>
    //               </li>
    //               <li className="nav-item">
    //                 <a className="nav-link text-white">
    //                   <span className="d-none d-sm-inline">Your Receipts</span>
    //                 </a>
    //               </li>
    //             </ul>
    //           </li>
    //         </ul>
    //       </div>
    //       <div className="dropdown open">
    //         <a
    //           className="btn border-none dropdown-toggle text-white"
    //           type="button"
    //           id="triggerId"
    //           data-bs-toggle="dropdown"
    //           aria-haspopup="true"
    //           aria-expanded="false"
    //         >
    //           <i className="bi bi-person-circle f5-4"></i>
    //           <span className="fs-5 ms-3 d-none d-sm-inline">{username}</span>
    //         </a>
    //         <div className="dropdown-menu" aria-labelledby="triggerId">
    //           <a className="dropdown-item">Edit Profile</a>
    //           <a
    //             className="dropdown-item text-danger"
    //             onClick={logout}
    //             href="/"
    //           >
    //             Logout
    //           </a>
    //         </div>
    //       </div>
    //     </div>
    //   </div>
    // </div>
  );
}
