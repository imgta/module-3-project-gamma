import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate, Link } from "react-router-dom";
import { useState, useRef, useEffect } from "react";

const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login, token } = useToken();
  const navigate = useNavigate();
  const userRef = useRef();

  const handleSubmit = (e) => {
    e.preventDefault();

    login(username, password);
    e.target.reset();
  };

  useEffect(() => {
    userRef.current.focus();

    if (token) {
      navigate("/dashboard");
    }
  }, [token, navigate]);

  return (
    <>
      <div className="bg-base-300">
        <h1 className="text-primary-focus text-5xl font-bold text-center pt-2">
          Welcome back!
        </h1>
        <p className="pb-4 pt-1 text-secondary text-center">
          Plan. Play. Connect.
        </p>
      </div>
      <div className="hero min-h-screen bg-base-300">
        <div className="hero-content flex-col lg:flex-row-reverse">
          <div className="lg:text-left text-base-content"></div>
          <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-200">
            <form onSubmit={handleSubmit}>
              <div className="card-body ">
                <div className="bg-base-200">
                  <label className="label text-base-content">Username</label>
                  <input
                    type="text"
                    ref={userRef}
                    name="username"
                    className="form-control input input-bordered"
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </div>
                <div className="bg-base-200">
                  <label className="label text-base-content text-lg">
                    Password
                  </label>
                  <input
                    name="password"
                    type="password"
                    onChange={(e) => setPassword(e.target.value)}
                    className="input input-bordered form-control"
                  />
                  <label className="label">
                    <Link
                      to="/"
                      className="label-text-alt link link-hover text-base-content"
                    >
                      Forgot password?
                    </Link>
                  </label>
                </div>
                <div className="mt-4 bg-base-200 flex justify-center">
                  <button
                    className="btn btn-primary btn-wide"
                    type="submit"
                    value="login"
                  >
                    Login
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginForm;
