import React, { useState, useEffect, useCallback } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import axios from "axios";

export default function GroupList() {
  const [myGroups, setMyGroups] = useState([]);
  const [username, setUsername] = useState("");
  const [userInitials, setUserInitials] = useState("");
  const { token } = useToken();

  const getUsername = useCallback(async () => {
    try {
      const res = await axios.get(`${process.env.REACT_APP_API_HOST}/user/me`, {
        headers: { Authorization: `Bearer ${token}` },
      });
      setUsername(res.data.username);
      const firstlast = res.data.fullname.split(" ");
      const initials = firstlast
        .map((name) => name.charAt(0).toUpperCase())
        .join("");
      setUserInitials(initials);
      const username = res.data.username;
      const groupUrl = `${process.env.REACT_APP_API_HOST}/groups/creator/${username}`;
      loadGroups(groupUrl);
    } catch (err) {
      console.log(err);
    }
  }, [token]);

  async function loadGroups(groupUrl) {
    try {
      const response = await fetch(groupUrl, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (response.ok) {
        const res = await response.json();
        setMyGroups(res);
      } else {
        console.error("Failed to fetch groups:", response.statusText);
      }
    } catch (error) {
      console.error("Error fetching groups:", error);
    }
  }

  async function handleDeleteGroup(name) {
    const confirmed = window.confirm(
      "Are you sure you want to delete this group?"
    );
    if (!confirmed) {
      return;
    }

    try {
      await axios.delete(
        `${process.env.REACT_APP_API_HOST}/api/groups/${name}/delete`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );
      loadGroups(
        `${process.env.REACT_APP_API_HOST}/groups/creator/${username}`
      );
    } catch (error) {
      console.error("Error deleting group:", error);
    }
  }

  useEffect(() => {
    if (token) {
      loadGroups();
      getUsername();
    }
  }, [token]);

  return (
    <div className="container">
      <h2>Your Groups</h2>
      <table className="table">
        <thead>
          <tr>
            <th>Group Name</th>
            <th>Description</th>
            <th>Created On</th>
            <th>Delete?</th>
          </tr>
        </thead>
        <tbody>
          {myGroups.map((group) => (
            <tr key={group.id}>
              <td>
                <b>{group.name}</b>
              </td>
              <td>
                <b>{group.description}</b>
              </td>
              <td>
                <b>{group.created_on}</b>
              </td>
              <td>
                <button
                  onClick={() => handleDeleteGroup(group.name)}
                  className="btn btn-error m-2"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="20"
                    height="20"
                    fill="#000000"
                    viewBox="0 0 256 256"
                  >
                    <path d="M216,48H176V40a24,24,0,0,0-24-24H104A24,24,0,0,0,80,40v8H40a8,8,0,0,0,0,16h8V208a16,16,0,0,0,16,16H192a16,16,0,0,0,16-16V64h8a8,8,0,0,0,0-16ZM96,40a8,8,0,0,1,8-8h48a8,8,0,0,1,8,8v8H96Zm96,168H64V64H192ZM112,104v64a8,8,0,0,1-16,0V104a8,8,0,0,1,16,0Zm48,0v64a8,8,0,0,1-16,0V104a8,8,0,0,1,16,0Z"></path>
                  </svg>
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
