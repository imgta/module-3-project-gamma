import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import React, { useState } from "react";

export default function CreateGroup() {
  const [groupName, setGroupName] = useState("");
  const [description, setDescription] = useState("");
  const { token } = useToken();
  const navigate = useNavigate();

  const groupNameChange = (event) => {
    setGroupName(event.target.value);
  };

  const descriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name: groupName,
      description: description,
    };

    const groupUrl = `${process.env.REACT_APP_API_HOST}/groups`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };

    const groupResponse = await fetch(groupUrl, fetchConfig);
    if (groupResponse.ok) {
      setGroupName("");
      setDescription("");
      navigate("/group-list");
    }
  };

  return (
    <div className="container">
      <h2 className="card-header">Create A Group</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="exampleFormControlInput1">Group Name:</label>
          <input
            type="text"
            className="form-control"
            id="exampleFormControlInput1"
            placeholder="Group Name"
            value={groupName}
            onChange={groupNameChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="exampleFormControlTextarea1">Description</label>
          <textarea
            className="form-control"
            id="exampleFormControlTextarea1"
            rows="3"
            value={description}
            onChange={descriptionChange}
          ></textarea>
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
}
