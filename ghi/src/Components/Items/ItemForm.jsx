import React, { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";

const ItemForm = () => {
    const [itemName, setItemName] = useState('');
    const [qty, setQty] = useState(1);
    const [cost, setCost] = useState(0);
    const [receipts, setReceipts] = useState([]);
    const [receiptId, setReceiptId] = useState(1);
    const { token } = useToken();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.item_name = itemName;
    data.item_qty = qty;
    data.item_cost = cost;
    data.receipt_id = receiptId;
    console.log(data);

    const itemsUrl = `${process.env.REACT_APP_API_HOST}/items`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };


        const response = await fetch(itemsUrl, fetchConfig);
        if (response.ok) {
            const newItem = await response.json();
            setItemName('');
            setQty(1);
            setCost(0);
            setReceiptId(1)

    }
  };

  const fetchData = async () => {
    const headers = { Authorization: `Bearer ${token}` };
    console.log(headers);
    const url = `${process.env.REACT_APP_API_HOST}/receipts`;
    console.log(url);

    const response = await fetch(url, headers);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setReceipts(data);
    }
  };

  useEffect(() => {
    if (token) {
      fetchData();
    }
  }, [token]);

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add an item</h1>
                    <form onSubmit={handleSubmit}>
                        <label>
                        Item Name:
                        <input
                            name="item name"
                            type="text"
                            value={itemName}
                            onChange={(e) => setItemName(e.target.value)}
                            className="input input-bordered form-control"
                            />
                        </label>

                        <label>
                        Quantity:
                        <input
                            type="number"
                            value={qty}
                            onChange={(e) => setQty(Number(e.target.value))}
                            className="input input-bordered form-control"
                            />
                        </label>

                        <label>
                        Cost:
                        <input
                            type="number"
                            value={cost}
                            onChange={(e) => setCost(parseFloat(e.target.value))}
                            className="input input-bordered form-control"
                            />
                        </label>
                        <div>
                        <label>
                        Choose a Receipt
                        <div className="mb-3">
                        <select value={receiptId} onChange={(e) => setReceiptId(Number(e.target.value))} className="form-select">
                            {receipts?.map(receipt => {
                                return (
                                    <option key={receipt.id} value={receipt.id}>
                                        { receipt.receipt_name }
                                    </option>
                                );
                            })}
                        </select>
                        </div>
                        </label>
                        </div>
                        <br />
                        <button type="submit" className="btn btn-dark">Add Item</button>
                    </form>
                </div>
            </div>
        </div>
    );
};


export default ItemForm;
