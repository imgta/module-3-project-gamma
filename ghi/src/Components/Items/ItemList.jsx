import React from 'react';
import { useEffect, useState } from 'react';
import useToken from '@galvanize-inc/jwtdown-for-react';
import axios from 'axios';

function ItemList() {
  const [items, setItems] = useState([]);
  const { token } = useToken();

  const fetchData = async () => {
    try {
      const itemListUrl = `${process.env.REACT_APP_API_HOST}/items`;
      const response = await fetch(itemListUrl, {
				method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (response.ok) {
        const data = await response.json();
        setItems(data);
      } else {
        console.error('Failed to fetch items:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching items:', error);
    }
  };

  useEffect(() => {
    if (token) {
      fetchData();
    }
  }, [token]);

  // If the token is not available, return null to not render the table
  if (!token) {
    return null;
  }

  return (
    <div className="container">
      <h2>Your Items</h2>
      <table className="table">
        <thead>
          <tr>
            <th>Item</th>
            <th>Quantity</th>
            <th>Cost</th>

            {/* figure out how to get the associated name of the receipt from the item */}
            <th>Receipt</th>
          </tr>
        </thead>
        <tbody>
          {items?.map((item) => {
            return (
              <tr key={item.id}>
                <td>{item.item_name}</td>
                <td>{item.item_qty}</td>
                <td>{item.item_cost}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ItemList;
