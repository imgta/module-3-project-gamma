import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import React from "react";
import RegisterForm from "./Components/Users/Register";
import LoginForm from "./Components/Users/Login";
import "./App.css";
import MainPage from "./Main";
import Nav from "./Nav";
import UserDashboard from "./Components/Users/UserDashboard";
import CreateGroup from "./Components/Groups/CreateGroup";
import GroupList from "./Components/Groups/GroupList";
import ReceiptForm from "./Components/Receipts/ReceiptForm";
import ReceiptList from "./Components/Receipts/ReceiptList";
import ItemForm from "./Components/Items/ItemForm";
import ItemList from "./Components/Items/ItemList";

function App() {
  const baseUrl = process.env.REACT_APP_API_HOST;
  console.log("baseUrl:", baseUrl);

  return (
    <BrowserRouter baseUrl={baseUrl}>
      <AuthProvider baseUrl={baseUrl}>
        <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="signup" element={<RegisterForm />}></Route>
          <Route path="login" element={<LoginForm />}></Route>
          <Route path="/create-group" element={<CreateGroup />}></Route>
          <Route path="/group-list" element={<GroupList />}></Route>
          <Route path="/dashboard" element={<UserDashboard />}></Route>
          <Route path="/create-receipt" element={<ReceiptForm />}></Route>
          <Route path="/receipt-list" element={<ReceiptList />}></Route>
          <Route path="/create-item" element={<ItemForm />}></Route>
          <Route path="/item-list" element={<ItemList/>}></Route>
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
