# BackTab

## Description

The BackTab Application is a web-based tool that allows users to easily split bills and expenses among a group of friends, roommates, or colleagues. It simplifies the process of dividing shared expenses, ensuring everyone pays their fair share.

The application is built using React for the frontend, FastAPI for the backend, and PostgreSQL for the database. It leverages the power of modern web technologies to provide a seamless and user-friendly experience.

## Features

- Create a new group for bill splitting.
- Add expenses with details like amount, description, and participants.
- Automatically calculate each participant's share of the expense.
- View a summary of all expenses and the total amount owed by each participant.
- Edit or delete existing expenses and update the bill sharing accordingly.
- Secure user authentication and authorization with Galvanize authentication.

## Authentication

BackTab provides uses the provided `UserAuthenticator` module for secure user authentication and authorization with Galvanize authentication. The `UserAuthenticator` class extends the `Authenticator` class from the `jwtdown_fastapi.authentication` module and handles the authentication process using JSON Web Tokens (JWT).

### Sign Up

To sign up for an account:

1. Click on the "Sign Up" button on the application's homepage.
2. Provide the required information, such as fullname, username, email, password, and birthdate.
3. Submit the sign-up form to create your account securely.

### Login

To log in to the application:

1. Click on the "Login" button on the application's navigation bar.
2. Enter your registered username and password.
3. Upon successful authentication, you will be redirected to your user dashboard.

### Authentication and Authoriztion Flow

BackTab uses token-based authentication to ensure secure communication between the frontend and backend. After successful login, the server issues an authentication token, which the client stores securely in local storage or cookies. This token is sent with each subsequent request to the backend to authenticate the user and grant access to authorized endpoints.

Please note that certain actions within the application may require specific user permissions. Unauthorized users will be restricted from accessing protected routes and performing sensitive operations.

## Installation

1. Clone the repository from GitLab:

```bash
   git clone https://gitlab.com/team-200-ok/module-3-project-gamma
```

2. Change into the project directory

```bash
  cd module-3-project-gamma
```

3. Install Frontend Dependencies

```bash
   cd ghi
   npm install
```

4. Install Backend Dependencies

```bash
   cd api
   pip install -r requirements.txt
```

### Configuration

Before running the application, you need to set up some configuration variables:

- Create a .env file in the api directory and specify the required environment variables like database connection details and other settings.
- To gernerate signing key type this code "openssl rand -hex 64"
- To generate the keys you must get the api keys via creating your own account to retrieve the keys

```plaintext
REACT_APP_API_HOST = http://localhost:8000
SIGNING_KEY = 'generated key'
DATABASE_URL = postgresql://postgres:postgres@postgres-db/backtab_db
VISION_API_KEY = 'generated key'
GOOGLE_CLOUD_KEY = generated key
AMAZON_S3_ACCESS_KEY = generated key
AMAZON_S3_SECRET_KEY = generated key
```

#### Usage

To start the server you must have Docker installed on your device. Once you have Docker, you must build the Docker image for the application and then create the database volumes. The backend server will be running on localhost:8000 and the frontend server will be running on localhost:3000.

1. Build the Docker images

```bash
docker compose build
```

2. Create the volumes

```bash
docker volume create gamma-data
```

```bash
docker volume create pg-admin
```

3. Build the Docker containers

```bash
docker compose up
```

## Contact

If you have any questions or need support, feel free to reach out to one if the developers.

- Dimitri Legaspi | dimitri.legaspi@gmail.com | https://gitlab.com/Dimitri.legaspi

- Gordon Ta | gphamta@gmail.com | https://gitlab.com/imgta

- Jisun Lee | jisun6560@gmail.com | https://gitlab.com/jisun-nyc

- Isaac Mendez | isaacmendez47@gmail.com | https://gitlab.com/isaacmendez47

---

Thank you for using BackTab! We hope it helps simplify your bill sharing experience. If you encounter any issues or have any feedback, please let us know. Happy bill splitting!
