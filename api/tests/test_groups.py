from fastapi.testclient import TestClient
from main import app
from datetime import date
from models.groups import Group
from queries.groups import GroupQueries

client = TestClient(app=app)


class FakeGroupQueries:
    def get_group_by_id(self, id):
        return Group(
            id=1,
            name="string",
            creator="string",
            description="string",
            invite_link="string",
            created_on=date(2023, 7, 20),
        )


def test_get_group_by_id():
    app.dependency_overrides[GroupQueries] = FakeGroupQueries

    response = client.get("/groups/id/1")

    app.dependency_overrides = {}

    assert response.status_code == 200
