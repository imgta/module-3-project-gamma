from models.groups import GroupCreate, Group
from routers.users import get_current_user
from fastapi.testclient import TestClient
from queries.groups import GroupQueries
from datetime import date
from main import app


client = TestClient(app)


class TestGroupQueries(GroupQueries):
    @staticmethod
    def invite_str():
        return "HQrKzG8800"

    def create_group(self, group, creator):
        invite_token = self.invite_str()
        return Group(
            id=1,
            name=group.name,
            creator=creator,
            description=group.description,
            invite_link=invite_token,
            created_on=date(2000, 1, 1),
        )


def test_get_current_user():
    return {
        "id": 1,
        "fullname": "first last",
        "email": "user1@email.com",
        "username": "user1",
        "birthdate": "2023-07-25",
    }


app.dependency_overrides = {
    GroupQueries: TestGroupQueries,
    get_current_user: test_get_current_user,
}


def test_create_group():
    test_group = GroupCreate(name="GroupTEST", description="descriptionTEST")
    res = client.post("/groups", json=test_group.dict())

    res_json = res.json()
    assert res_json["creator"] == "user1"
    assert res_json["invite_link"] == "HQrKzG8800"
