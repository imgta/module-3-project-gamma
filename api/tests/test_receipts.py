from fastapi.testclient import TestClient
from authenticator import authenticator
from main import app
from models.receipts import ReceiptOut
from models.users import UserOut
from datetime import date
from queries.receipts import ReceiptQueries


client = TestClient(app=app)


class FakeReceiptQueries:
    def create_receipt(self, id):
        return ReceiptOut(
            id=1,
            venue_name="Supermarket",
            address="123 Lane",
            total_cost=123.00,
            transaction_date=date(2023, 7, 20),
            assigned_group=1,
            settled=False,
            receipt_name="Whole Foods",
        )


def get_current_user():
    return UserOut(
        id=1,
        fullname="Bob Bob",
        email="bob@geats.com",
        username="Bob123",
        birthdate=date(2000, 1, 1),
    )


def test_create_receipt():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_user

    app.dependency_overrides[ReceiptQueries] = FakeReceiptQueries

    response = client.post(
        "/receipts",
        json={
            "venue_name": "Supermarket",
            "address": "123 Lane",
            "total_cost": 123.00,
            "image_url": "https://t3.ftcdn.net/jpg/01/82/01/18/360_F_182011806_mxcDzt9ckBYbGpxAne8o73DbyDHpXOe9.jpg",
            "transaction_date": "2023-07-20",
            "assigned_group": 1,
            "settled": False,
            "receipt_name": "Whole Foods",
        },
    )

    app.dependency_overrides = {}

    assert response.status_code == 200
