from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from models.users import UserOut
from datetime import date

client = TestClient(app=app)


def get_current_account_data():
    return UserOut(
        id=1,
        fullname="Bob Bob",
        email="bob@geats.com",
        username="Bob123",
        birthdate=date(2000, 1, 1),
    )


def test_get_current_user():
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_account_data

    expected = {
        "id": 1,
        "fullname": "Bob Bob",
        "email": "bob@geats.com",
        "username": "Bob123",
        "birthdate": "2000-01-01",
    }
    response = client.get("/user/me")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
