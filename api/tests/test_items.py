from fastapi.testclient import TestClient
from main import app
from queries.items import ItemQueries


client = TestClient(app=app)


class FakeItemQueries:
    def delete(self, id):
        return True


def test_delete():
    app.dependency_overrides[ItemQueries] = FakeItemQueries

    response = client.delete("/items/1")
    app.dependency_overrides = {}

    assert response.status_code == 200
