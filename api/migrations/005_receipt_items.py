steps = [
    [
        """
		CREATE TABLE receipt_items (
			id SERIAL PRIMARY KEY NOT NULL,
            receipt_id INT NOT NULL REFERENCES receipts(id) ON DELETE CASCADE,
			item_name VARCHAR(1000) NOT NULL,
			item_qty INT DEFAULT 1,
			item_cost DECIMAL(10, 2) NOT NULL
		);
		""",
        """
		DROP TABLE receipt_items;
		""",
    ]
]
