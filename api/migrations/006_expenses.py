steps = [
    [
        """
		CREATE TABLE expenses (
			id SERIAL PRIMARY KEY NOT NULL,
			item_id INT NOT NULL REFERENCES receipt_items(id) ON DELETE CASCADE,
            ordered_by TEXT[] NOT NULL,
            paid_by TEXT[] NOT NULL,
			amt_owed DECIMAL(10, 2) NOT NULL,
            settled BOOLEAN DEFAULT FALSE
		);
		""",
        """
		DROP TABLE expenses;
		""",
    ]
]
