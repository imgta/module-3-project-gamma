steps = [
    [
        """
		CREATE TABLE receipts (
			id SERIAL PRIMARY KEY NOT NULL,
			venue_name VARCHAR(1000) NOT NULL,
			address VARCHAR(1000),
			total_cost DECIMAL(10, 2) NOT NULL,
            image_url TEXT,
			transaction_date DATE,
			assigned_group INT REFERENCES groups(id),
			settled BOOLEAN DEFAULT FALSE,
			receipt_name VARCHAR(200),
			assigned_group_name VARCHAR(200)
    	);
		""",
        """
		DROP TABLE receipts;
		""",
    ]
]
