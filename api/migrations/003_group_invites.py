steps = [
    [
        """
        CREATE TYPE STATUS AS ENUM ('ACCEPTED', 'INVITED', 'REJECTED');
        CREATE TABLE group_invites (
            id SERIAL PRIMARY KEY NOT NULL,
            group_id INT NOT NULL REFERENCES groups(id) ON DELETE CASCADE,
            invited_user VARCHAR(200) NOT NULL REFERENCES users(username) ON DELETE CASCADE,
            status STATUS DEFAULT 'INVITED'
        );
        """,
        """
        DROP TABLE group_invites;
        DROP TYPE STATUS;
        """,
    ]
]
