steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            fullname VARCHAR(200) NOT NULL,
            email VARCHAR(200) NOT NULL UNIQUE,
            username VARCHAR(200) NOT NULL UNIQUE,
            hashed_password VARCHAR(200) NOT NULL,
            birthdate DATE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """,
    ]
]
