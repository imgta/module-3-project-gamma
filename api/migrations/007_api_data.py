steps = [
    [
        """
		CREATE TABLE api_data (
			id SERIAL PRIMARY KEY NOT NULL,
            receipt_id INT NOT NULL REFERENCES receipts(id) ON DELETE CASCADE,
			item_name VARCHAR(1000) NOT NULL,
			item_quantity INT DEFAULT 1,
			item_cost DECIMAL(10, 2) NOT NULL,
            assigned_user VARCHAR(200) REFERENCES users(username)
		);
		""",
        """
		DROP TABLE api_data;
		""",
    ]
]
