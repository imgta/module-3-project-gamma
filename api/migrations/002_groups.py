steps = [
    [
        """
        CREATE TABLE groups (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(200) NOT NULL,
            creator VARCHAR(200) NOT NULL REFERENCES users(username),
            description TEXT,
            invite_link TEXT,
            created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        );
        """,
        """
        DROP TABLE groups;
        """,
    ]
]
