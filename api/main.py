from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
from fastapi import FastAPI
from pydantic import BaseModel
from routers import users, groups, group_invites, receipts, items
import os


app = FastAPI()

routers = [
    authenticator.router,
    users.router,
    groups.router,
    group_invites.router,
    receipts.router,
    items.router,
]
for router in routers:
    app.include_router(router)


class Data(BaseModel):
    user: str


@app.post("/")
def main(data: Data):
    return data


origins = [
    os.environ.get("CORS_HOST"),
    "https://team-200-ok.gitlab.io",
    "https://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }
