from models.users import User
from jwtdown_fastapi.authentication import Authenticator
from datetime import datetime, date
from queries.users import UserQueries
from fastapi import Depends
import json
import os


class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        return super().default(obj)


class UserAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        users: UserQueries,
    ):
        # Use your repo to get the user based on the username
        # (which could be an email)
        return users.get_by_username(username)

    # Return the users. That's it.
    def get_account_getter(
        self,
        users: UserQueries = Depends(),
    ):
        return users

    # Return the encrypted password value from your user object
    def get_hashed_password(self, user: User):
        return user.hashed_password

    # Return the username and the data for the cookie.
    # You must return TWO values from this method.
    def get_account_data_for_cookie(self, user: User):
        user_dict = user.dict()
        return user.username, json.loads(
            json.dumps(user_dict, cls=DateTimeEncoder)
        )
        # return user.username, UserOut(**user.dict())


# use environ.get() if error
authenticator = UserAuthenticator(os.environ["SIGNING_KEY"])
