# from fastapi import (
#     HTTPException,
#     UploadFile,
#     APIRouter,
#     Depends,
#     status,
#     File,
# )
# from models.receipts import ReceiptFormUpload

# from queries.receipts import ReceiptQueries
# from routers.users import get_current_user

# from queries.groups import GroupQueries

# from botocore.config import Config
# from models.users import UserOut
# from dotenv import load_dotenv

# import boto3
# import os

# router = APIRouter()

# load_dotenv()
# s3_access_key = os.getenv("AWS_ACCESS_KEY_ID")
# s3_secret_key = os.getenv("AWS_SECRET_ACCESS_KEY")
# s3_region = os.getenv("AWS_DEFAULT_REGION")
# s3_bucket_name = "backtab-receipts"


# s3 = boto3.client(
#     "s3",
#     region_name=s3_region,
#     aws_access_key_id=s3_access_key,
#     aws_secret_access_key=s3_secret_key,
#     config=Config(signature_version="s3v4"),
# )

# result = s3.get_bucket_policy(Bucket=s3_bucket_name)
# print(result["Policy"])


# #### TEST #####
# @router.post("/image/upload")
# async def upload_image(
#     image: UploadFile = File(...),
# ):
#     if image:
#         print("File to Upload:", image.filename)
#         s3.upload_fileobj(
#             image.file,
#             s3_bucket_name,
#             image.filename,
#         )
#         img_url = f"https://{s3_bucket_name}.s3.us-east-2.amazonaws.com/{image.filename}"
#         print({"message": "Successfully uploaded your receipt!"})
#         print(f"URL: {img_url}")
#         return ("Upload successful!", img_url)
#     else:
#         return "Upload failed!"


# @router.post("/receipts/upload", tags=["RECEIPTS"])
# async def create_upload_receipt(
#     image: UploadFile = File(...),
#     form: ReceiptFormUpload = Depends(),
#     current_user: UserOut = Depends(get_current_user),
#     group_qry: GroupQueries = Depends(),
#     repo: ReceiptQueries = Depends(),
# ):
#     image_url = "imageUrl"
#     # Check if user is logged in
#     if not current_user:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail="You must be logged in to create receipts.",
#         )
#     # Check if user-provided assigned_group exists
#     group = group_qry.get_group_by_id(form.assigned_group)
#     if group is None:
#         raise HTTPException(
#             status_code=status.HTTP_404_NOT_FOUND,
#             detail="Group ID not found.",
#         )
#     # Only allow user to create receipts for groups in which they are members
#     current_user = current_user["username"]
#     membership = group_qry.get_group_memberships(current_user)
#     group_list = [int(entry["group_id"]) for entry in membership[current_user]]
#     print(f"Group LIST:{group_list}")
#     if form.assigned_group not in group_list:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail="You are not authorized to create receipts for this group.",
#         )
#     if image:
#         print("File to upload:", image.filename)
#         s3.upload_fileobj(
#             image.file,
#             s3_bucket_name,
#             image.filename,
#         )
#         image_url = f"https://{s3_bucket_name}.s3.us-east-2.amazonaws.com/{image.filename}"
#         print({"message": "Successfully uploaded your receipt!"})
#         print(f"URL: {image_url}")
#     else:
#         return "Receipt upload failed."
#     new_receipt = repo.create_receipt_upload(form, image_url)
#     return new_receipt
