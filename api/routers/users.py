from authenticator import authenticator
from fastapi.security import oauth2
from queries.users import UserQueries
from models.users import (
    HttpError,
    UserToken,
    UserForm,
    UserOut,
    UserOutWithPassword,
    UserIn,
    UserUpdate,
    User,
)
from typing import List
from fastapi import (
    HTTPException,
    APIRouter,
    Response,
    Depends,
    Request,
    status,
)

router = APIRouter()


@router.post("/users", response_model=UserToken | HttpError, tags=["USERS"])
async def create_user(
    info: UserIn,
    request: Request,
    response: Response,
    repo: UserQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    user = repo.user_create(info, hashed_password)
    form = UserForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    return UserToken(user=user, **token.dict())


@router.get("/user/me", response_model=UserOut, tags=["USERS"])
def get_current_user(
    current_user: UserOut = Depends(authenticator.get_current_account_data),
):
    return current_user


@router.get("/users/all", response_model=List[UserOut], tags=["USERS"])
def get_all_users(user: UserQueries = Depends()):
    return user.get_all_users()


@router.get("/users/{username}", response_model=UserOut, tags=["USERS"])
def get_by_username(
    username: str,
    response: Response,
    query: UserQueries = Depends(authenticator.get_account_getter),
):
    row = query.get_by_username(username)
    if row is None:
        response.status_code = 404
    else:
        return row


@router.put(
    "/users/update", response_model=UserOutWithPassword, tags=["USERS"]
)
def update_user(
    auth: oauth2.OAuth2PasswordRequestForm = Depends(),
    fullname: str = None,
    email: str = None,
    new_password: str = None,
    form: UserUpdate = Depends(),
    current_user: UserOut = Depends(get_current_user),
    repo: UserQueries = Depends(),
):
    username = current_user["username"]
    user_fullname = current_user["fullname"]
    user_email = current_user["email"]
    current_pw = current_user["hashed_password"]

    # Check if password input matches existing hashed password for user
    if not authenticator.pwd_context.verify(auth.password, current_pw):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect password for current user.",
        )
    form.fullname = user_fullname
    form.email = user_email
    form.new_password = current_pw

    if fullname is not None:
        form.fullname = fullname
    if email is not None:
        form.email = email
    if new_password is not None:
        new_pw = authenticator.hash_password(new_password)
        form.new_password = new_pw
    updated_user = repo.user_update(form, username)
    if updated_user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User not found",
        )
    return updated_user


@router.get("/token", response_model=UserToken | None, tags=["USERS"])
async def get_token(
    request: Request,
    user: User = Depends(authenticator.try_get_current_account_data),
) -> UserToken | None:
    if user and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "user": user,
        }


@router.get("/api/protected", response_model=bool, tags=["USERS"])
async def get_protected(
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return True
