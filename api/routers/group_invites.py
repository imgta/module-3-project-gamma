from queries.group_invites import GroupInviteQueries
from routers.users import get_current_user
from models.groups import GroupOut
from queries.groups import GroupQueries
from queries.users import UserQueries
from models.group_invites import (
    UserGroupsList,
    InviteStatus,
    InviteList,
    InvitesOut,
    Invites,
)
from models.users import UserOut
from typing import List
from fastapi import (
    HTTPException,
    APIRouter,
    Depends,
    status,
)


router = APIRouter()


@router.get("/api/invites", response_model=InviteList, tags=["INVITES"])
def get_all_invites(query: GroupInviteQueries = Depends()):
    row = query.get_all_invites()
    if row is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No group invites found.",
        )
    return row


@router.post(
    "/invites",
    response_model=Invites,
    tags=["INVITES"],
)
def create_invite(
    form: InvitesOut,
    current_user: UserOut = Depends(get_current_user),
    repo: GroupInviteQueries = Depends(),
    group_qry: GroupQueries = Depends(),
    user_qry: UserQueries = Depends(),
):
    # Check if user is logged in
    if not current_user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to craft invites.",
        )
    # Check if group id exists in groups
    group = group_qry.get_group_by_id(form.group_id)
    if group is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Group ID not found.",
        )
    # Check if group invite creator is creator of said group
    if group.creator != current_user["username"]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Only the creator of this group can invite others.",
        )
    # Check if invited user exists in users
    invited_user = user_qry.get_by_username(form.invited_user)
    if invited_user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User {invited_user} does not exist.",
        )
    # Check if invited user has already been invited to the group
    user_invites = repo.invites_for_user(form.invited_user)
    if user_invites is not None:
        for old in user_invites:
            if old["group_id"] == form.group_id:
                raise HTTPException(
                    status_code=status.HTTP_409_CONFLICT,
                    detail="{invited_user} has already been invited to the group!",
                )
    new_invite = repo.create_invite(form, InviteStatus.INVITED)
    return new_invite


@router.get("/invites/id/{id}", response_model=Invites, tags=["INVITES"])
def get_invite_by_id(
    id: int, query: GroupInviteQueries = Depends()
) -> Invites:
    row = query.get_invite_by_id(id)
    if row is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Invite id not found.",
        )
    return row


@router.get(
    "/invites/groupid/{group_id}",
    response_model=InviteList,
    tags=["INVITES"],
)
def invites_by_groupid(
    group_id: int, query: GroupInviteQueries = Depends()
) -> Invites:
    row = query.invites_by_groupid(group_id)
    if row is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No invites from {group_id}.",
        )
    return row


@router.get(
    "/invites/user/{invited_user}",
    response_model=InviteList,
    tags=["INVITES"],
)
def invites_for_invited_user(
    invited_user: str, query: GroupInviteQueries = Depends()
) -> Invites:
    row = query.invites_for_user(invited_user)
    if row is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="{invited_user} has not been invited to any groups yet :(",
        )
    return row


def extract_group_data(
    invited_user: str, record: List[dict], groups: List[GroupOut]
) -> UserGroupsList:
    group_ids = [row["group_id"] for row in record]
    user_info = {"username": invited_user, "groups": []}
    for group in groups:
        if group.id in group_ids:
            matching_record = next(
                (
                    row
                    for row in record
                    if row["group_id"] == group.id
                    and row["invited_user"] == invited_user
                ),
                None,
            )
            if matching_record:
                invite_status = matching_record["status"]
        group_info = {
            "group_name": group.name,
            "invite_status": invite_status,
        }
        user_info["groups"].append(group_info)
    return user_info


@router.get(
    "/groups/user/{invited_user}",
    tags=["GROUPS"],
)
def user_in_groups_by_username(
    invited_user: str,
    query: GroupInviteQueries = Depends(),
    group_qry: GroupQueries = Depends(),
):
    record = query.invites_for_user(invited_user)
    if record is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User has not been invited to any groups yet :(",
        )
    groups = group_qry.get_all_groups()
    user_groups = extract_group_data(invited_user, record, groups)
    return user_groups
