from fastapi import APIRouter, Depends, Response, HTTPException, status
from queries.receipts import ReceiptQueries
from queries.groups import GroupQueries
from typing import List, Optional
from models.receipts import Receipt, ReceiptCreate, ReceiptOut
from models.users import UserOut
from routers.users import get_current_user


router = APIRouter()


@router.post("/receipts", response_model=ReceiptOut, tags=["RECEIPTS"])
def create_receipts(
    receipt: Receipt,
    response: Response,
    current_user: UserOut = Depends(get_current_user),
    repo: ReceiptQueries = Depends(),
):
    if not current_user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to create_receipt receipts.",
        )
    created_receipt = repo.create_receipt(receipt)
    if created_receipt is None:
        response.status_code = 400
    return created_receipt


@router.post("/receipts", response_model=ReceiptCreate, tags=["RECEIPTS"])
def create_receipt_item(
    receipt: ReceiptCreate,
    response: Response,
    repo: ReceiptQueries = Depends(),
    group_repo: GroupQueries = Depends(),
    current_user: UserOut = Depends(get_current_user),
):
    assigned_group = group_repo.get_group_by_id(receipt.assigned_group)
    if not assigned_group:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Assigned group could not be found",
        )
    # return assigned_group

    created_receipt = repo.create_receipt(receipt, assigned_group.name)
    if not created_receipt:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Could not create receipt",
        )
    return created_receipt


@router.get("/receipts", response_model=List[ReceiptOut], tags=["RECEIPTS"])
def get_all_receipts(response: Response, repo: ReceiptQueries = Depends()):
    all_receipts = repo.get_all_receipts()
    if not all_receipts:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No receipts found",
        )
    return all_receipts


@router.get(
    "/receipts/{receipt_id}",
    response_model=Optional[ReceiptOut],
    tags=["RECEIPTS"],
)
def get_one_receipt(
    receipt_id: int, response: Response, repo: ReceiptQueries = Depends()
) -> ReceiptOut:
    receipt = repo.get_one(receipt_id)
    if not receipt:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Receipt with the id {receipt_id} not found",
        )
    return receipt


@router.delete(
    "/receipts/{receipt_id}", response_model=bool, tags=["RECEIPTS"]
)
def delete_receipt(receipt_id: int, repo: ReceiptQueries = Depends()) -> bool:
    if not repo.delete_receipt(receipt_id):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Receipt not found"
        )
    return True


@router.put("/receipts/{receipt_id}", tags=["RECEIPTS"])
def update_receipt(
    receipt_id: int, receipt: Receipt, repo: ReceiptQueries = Depends()
) -> ReceiptOut:
    update_receipt = repo.update_receipt(receipt_id, receipt)
    if not update_receipt:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Receipt id:{id} could not be created",
        )
    return update_receipt
