from redmail import EmailSender, gmail
from dotenv import load_dotenv
import os

load_dotenv()
smtp_host = os.getenv("SMTP_EMAIL_HOST")
smtp_port = os.getenv("SMTP_PORT")
smtp_username = os.getenv("SMTP_USERNAME")  # Gmail Address
smtp_password = os.getenv("SMTP_PASSWORD")  # Gmail App Password
smtp_sender = os.getenv("SMTP_USERNAME")

email = EmailSender(
    host=smtp_host,
    port=smtp_port,
    username=smtp_password,
    password=smtp_password,
)

gmail.username = smtp_username
gmail.password = smtp_password

sbj = "A New Invitation Awaits You!"
msg = gmail.get_message(
    subject=sbj,
    sender=smtp_sender,
    receivers=[smtp_username],
    text=f"Hello!\nYou've been invited to join a group. Click the following link to accept your invitation:\n\n<INVITE LINK>",
)

gmail.send(
    subject=sbj,
    sender=smtp_sender,
    receivers=[smtp_username],
    text=f"Hello!\nYou've been invited to join a group!\nClick the following link to accept your invitation:\n\n<INVITE LINK>",
)
