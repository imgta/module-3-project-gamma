from fastapi import APIRouter, Depends, Response, HTTPException, status
from queries.items import ItemQueries
from typing import List, Optional
from models.items import ItemIn, ItemOut

router = APIRouter()


@router.post("/items", response_model=ItemOut, tags=["ITEMS"])
def create_item(
    item: ItemIn, response: Response, repo: ItemQueries = Depends()
):
    created_item = repo.create(item)
    if not create_item:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Failed to create item",
        )
    return created_item


@router.get("/items", response_model=List[ItemOut], tags=["ITEMS"])
def get_all_items(response: Response, repo: ItemQueries = Depends()):
    items = repo.get_all()
    if not items:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No items found",
        )
    return items


@router.get(
    "/items/{item_id}", response_model=Optional[ItemOut], tags=["ITEMS"]
)
def get_one_item(
    item_id: int, response: Response, repo: ItemQueries = Depends()
) -> ItemOut:
    item = repo.get_one(item_id)
    if not item:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Item with the id {item_id} not found",
        )
    return item


@router.delete("/items/{item_id}", response_model=bool, tags=["ITEMS"])
def delete_item(
    item_id: int, response: Response, repo: ItemQueries = Depends()
) -> bool:
    item_deleted_true = repo.delete(item_id)
    return item_deleted_true


@router.put("/items/{item_id}", response_model=ItemOut, tags=["ITEMS"])
def update_item(
    response: Response,
    item_id: int,
    item: ItemIn,
    repo: ItemQueries = Depends(),
):
    updated_item = repo.update(item_id, item)
    if not update_item:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Item not found"
        )
    return updated_item
