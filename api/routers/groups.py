from models.groups import Group, GroupOutList, GroupCreate, GroupList
from routers.users import get_current_user
from queries.groups import GroupQueries
from models.users import UserOut
from fastapi import (
    HTTPException,
    APIRouter,
    Depends,
    status,
)

router = APIRouter()


@router.get("/groups/all", response_model=GroupOutList, tags=["GROUPS"])
def get_all_groups(query: GroupQueries = Depends()):
    groups = query.get_all_groups()
    if not groups:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="No Groups Found"
        )
    return groups


@router.post("/groups", response_model=Group, tags=["GROUPS"])
def create_group(
    group: GroupCreate,
    repo: GroupQueries = Depends(),
    current_user: UserOut = Depends(get_current_user),
):
    new_group = repo.create_group(group, current_user["username"])
    if not new_group:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Failed to create group",
        )
    return new_group


@router.get("/groups/user", tags=["GROUPS"])
def get_groups_for_user(
    username: str,
    query: GroupQueries = Depends(),
):
    groups = query.get_groups_for_username(username)
    if not groups:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No groups found for the user: {username}",
        )
    return groups


@router.get("/groups/me", tags=["GROUPS"])
def get_my_groups(
    query: GroupQueries = Depends(),
    current_user: UserOut = Depends(get_current_user),
):
    current_user = current_user["username"]
    my_groups = query.get_groups_for_username(current_user)
    if not my_groups:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No groups found for the current user: {current_user}.",
        )
    return my_groups


@router.get("/groups/id/{id}", response_model=Group, tags=["GROUPS"])
def get_group_by_id(id: int, query: GroupQueries = Depends()) -> Group:
    row = query.get_group_by_id(id)
    if row is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Group ID not found."
        )
    return row


@router.get(
    "/groups/creator/{creator}", response_model=GroupList, tags=["GROUPS"]
)
def groups_by_creator(creator: str, query: GroupQueries = Depends()) -> Group:
    row = query.groups_by_creator(creator)
    if row is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No groups by {creator} found.",
        )
    return row


@router.get(
    "/groups/name/{name}", response_model=GroupOutList, tags=["GROUPS"]
)
def get_group_by_name(name: str, query: GroupQueries = Depends()) -> Group:
    row = query.get_group_by_name(name)
    if row is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Group {name} not found.",
        )
    return row


@router.delete(
    "/api/groups/{name}/delete", response_model=str, tags=["GROUPS"]
)
def delete_group(name: str, query: GroupQueries = Depends()):
    group = query.get_group_by_name(name)
    if not group:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Group {name} not found.",
        )
    query.delete_group(name)
    return f"Group {name} successfully deleted"
