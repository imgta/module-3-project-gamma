from pillow_heif import register_heif_opener
from dotenv import load_dotenv
from io import BytesIO
import PIL.Image
import requests
import json
import os

load_dotenv()
maps_api_key = os.getenv("GOOGLE_CLOUD_KEY")

# Pillow library plugin to open and process .HEIF/.HEIC images
register_heif_opener()

imgUrl = "https://backtab-receipts.s3.us-east-2.amazonaws.com/sample.jpeg"
imgUrl2 = "https://backtab-receipts.s3.us-east-2.amazonaws.com/test.HEIC"


# Send HTTP GET request to URL for image content/data
def get_img_data(url):
    imgResponse = requests.get(imgUrl)
    imgData = imgResponse.content
    return imgData


# Extract GPS metadata from EXIF data, tag ID 0x8825 for GPS-related data
def gps_extract(img):
    exif = img.getexif().get_ifd(0x8825)

    # Establish latitude and longitude values
    if exif is not None:
        lat_direction = exif[1]
        lat_deg, lat_min, lat_sec = exif[2]
        long_direction = exif[3]
        long_deg, long_min, long_sec = exif[4]

        # Convert latitude and longitude values to decimal degrees
        lat_decimal = lat_deg + lat_min / 60 + lat_sec / 3600
        long_decimal = long_deg + long_min / 60 + long_sec / 3600

        if lat_direction == "S":  # Southern hemisphere adjustment
            lat_decimal *= -1
        if long_direction == "W":  # Western hemisphere adjustment
            long_decimal *= -1

        # Convert decimal degrees to floats, then set coordinates
        latitude, longitude = float(lat_decimal), float(long_decimal)
        coord = (latitude, longitude)
        return latitude, longitude


# Send HTTP GET request to Google Maps API, parse+clean JSON response
def get_address(latitude, longitude):
    mapsUrl = f"https://maps.googleapis.com/maps/api/geocode/json?latlng={latitude},{longitude}&key={maps_api_key}"
    mapsResponse = requests.get(mapsUrl)
    mapsData = mapsResponse.json()
    cleanMapsData = json.dumps(mapsData, indent=2)
    if mapsData["status"] == "OK" and len(mapsData["results"]) > 0:
        mapResult = mapsData["results"][0]
        address = mapResult["formatted_address"]
        return address


# Types of places that typically incur a bill
billTypes = [
    "accounting",
    "airport",
    "amusement_park",
    "aquarium",
    "art_gallery",
    "bakery",
    "bank",
    "bar",
    "beauty_salon",
    "bicycle_store",
    "book_store",
    "bowling_alley",
    "bus_station",
    "cafe",
    "car_dealer",
    "car_rental",
    "car_repair",
    "car_wash",
    "casino",
    "clothing_store",
    "convenience_store",
    "dentist",
    "department_store",
    "doctor",
    "drugstore",
    "electrician",
    "electronics_store",
    "florist",
    "funeral_home",
    "furniture_store",
    "gas_station",
    "gym",
    "hair_care",
    "hardware_store",
    "home_goods_store",
    "hospital",
    "insurance_agency",
    "jewelry_store",
    "laundry",
    "lawyer",
    "library",
    "liquor_store",
    "lodging",
    "meal_delivery",
    "meal_takeaway",
    "movie_theater",
    "movie_rental",
    "moving_company",
    "museum",
    "night_club",
    "painter",
    "parking",
    "pet_store",
    "pharmacy",
    "physiotherapist",
    "plumber",
    "post_office",
    "restaurant",
    "roofing_contractor",
    "shoe_store",
    "shopping_mall",
    "spa",
    "stadium",
    "storage",
    "store",
    "subway_station",
    "supermarket",
    "taxi_stand",
    "train_station",
    "travel_agency",
    "university",
    "veterinary_care",
    "zoo",
]


# Send HTTP GET request to Google Places API, parse+clean JSON response
def get_venues(latitude, longitude):
    placesUrl = f"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={latitude},{longitude}&radius=50&key={maps_api_key}"
    placesResponse = requests.get(placesUrl)
    placesData = placesResponse.json()
    placesResults = placesData["results"]
    return placesResults


def venues_by_distance(venues, latitude, longitude):
    # Function to calculate distance between two points
    def measureDistance(x1, y1, x2, y2):
        distance = ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5
        return distance

    # Most relevant venues within a 47m radius
    areaVenues = [
        {
            "geometry": venue["geometry"],
            "name": venue["name"],
            "types": venue["types"],
            "distance": measureDistance(
                latitude,
                longitude,
                venue["geometry"]["location"]["lat"],
                venue["geometry"]["location"]["lng"],
            ),
        }
        for venue in venues
        if any(type in venue["types"] for type in billTypes)
    ]
    # print(f"AREA VENUES: {json.dumps(areaVenues,indent=2)}")

    # Sort areaVenues by distance (closest to farthest)
    distanceSort = sorted(areaVenues, key=lambda loc: loc["distance"])

    # Names of nearby venues sorted by distance
    nearbyVenues = [venue["name"] for venue in distanceSort]
    return nearbyVenues


# Access image data
imgData = get_img_data(imgUrl)

# Convert binary image data into file-like object for PIL
img = PIL.Image.open(BytesIO(imgData))

# Extract GPS coordinates from image
latitude, longitude = gps_extract(img)

# Get location/address details
address = get_address(latitude, longitude)

# Get nearby venues, sort venues by distance, get nearest venue
venues = get_venues(latitude, longitude)
area_venues = venues_by_distance(venues, latitude, longitude)
nearest_venue = area_venues[0]

print(f"     VENUE: {nearest_venue}")
print(f"     ADDRESS: {address}")
print(f"     COORDINATES: {latitude}, {longitude}")
print(f"NEARBY VENUES: {area_venues}")
