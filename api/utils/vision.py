import requests
import base64
import json
import re
import os

vision_api_key = os.getenv("GOOGLE_CLOUD_KEY")

img_path = "./images/franks_receipt.png"
url = "https://vision.googleapis.com/v1/images:annotate?key=" + vision_api_key

# Load receipt image
with open(img_path, "rb") as image_file:
    content = image_file.read()

# Convert content to base64-encoded string to allow JSON serialization
content_base64 = base64.b64encode(content).decode("utf-8")

# Create request payload
payload = {
    "requests": [
        {
            "image": {"content": content_base64},
            "features": [{"type": "TEXT_DETECTION"}],
        }
    ]
}

# Send request to Cloud Vision API
res = requests.post(url, json=payload)
data = json.loads(res.text)

# Extract recognized text from the response
parsed_txt = data["responses"][0]["textAnnotations"][0]["description"]

# Split parsed text into list by line-breaks
txt_lines = parsed_txt.split("\n")

# Extract prices: numbers with two decimal places
price_format = r"\d+\.\d{2}\b(?!-|\.)"
prices = []
for line in txt_lines:
    price = re.match(price_format, line)
    if price:
        prices.append(price.group())

# Convert price numbers to floats for summing costs
costs = [float(num) for num in prices]
cost_len = len(costs)

total = max(costs)

charges = costs
idx_total = charges.index(total)


# List charges up to the last total line
charges = []
index_total = costs.index(float(total))
charges = costs[:index_total]

# Remove subtotal value from charges list
# subtotal = max(charges)
# if total_count > 1 and len(charges) > 1:
#     charges.remove(subtotal)

# Check if all charges sum up to total amount
if round(sum(charges), 2) == total:
    print("Charges are valid ✔")
else:
    print("Charges do not match total cost ❌")

# Establish date formatting patterns, extract transaction date from lines
# (?!-|\.) - negative lookahead (date matches are not followed by "-" or ".")
# \b - sets up boundaries
date_formats = [
    r"\b\d{2} [A-Za-z]{3,9} \d{4}\b(?!-|\.)",  # September DD, YYYY
    r"\b[A-Za-z]{3} \d{2}, \d{4}\b(?!-|\.)",  # Jun DD, YYYY
    r"\b\d{2} [A-Za-z]{3,9} \d{4}\b(?!-|\.)",  # DD September YYYY
    r"\b[A-Za-z]{3}\.\d{2}\'\d{2}\b(?!-|\.)",  # Jun.DD'YY
    r"\b[A-Za-z]{3}\d{2}'\d{2}\b(?!-|\.)",  # JunDD'YY
    r"\b\d{2}/\d{2}/\d{4}\b(?!-|\.)",  # DD/MM/YYYY
    r"\b\d{2}-\d{2}-\d{4}\b(?!-|\.)",  # DD-MM-YYYY
    r"\b\d{4}/\d{2}/\d{2}\b(?!-|\.)",  # YYYY/MM/DD
    r"\b\d{4}-\d{2}-\d{2}\b(?!-|\.)",  # YYYY-MM-DD
    r"\b\d{2}/\d{2}/\d{2}\b(?!-|\.)",  # DD/MM/YY
    r"\b\d{2}-\d{2}-\d{2}\b(?!-|\.)",  # DD-MM-YY
    r"\b\d{2}\.\d{2}\.\d{4}\b(?!-|\.)",  # DD.MM.YYYY
    r"\b\d{2}\.\d{2}\.\d{2}\b(?!-|\.)",  # DD.MM.YY
]

date = []
for line in txt_lines:
    for format in date_formats:
        match = re.findall(format, line)
        date.extend(match)


# Extract sales tax from lines
tax_rate = None
tax_format = r"(\d+(?:\.\d+)?)%"  # 1.0% or 10.0%
tax_keywords = ["tax", "sales tax", "tax rate"]
# Convert tax_keywords into RegEx pattern
tax_words = r"({})".format(
    "|".join(re.escape(keyword) for keyword in tax_keywords)
)

tax_patterns = r"{}|{}".format(tax_words, tax_format)

rate = re.findall(tax_format, parsed_txt, re.IGNORECASE)
if rate:
    tax_rate = rate[0]

# tax_charge = round(subtotal * (float(tax_rate)/100),2)
sum_charges = round(sum(charges), 2)

# print(parsed_txt)
print(txt_lines)
print("Date:", date)
print(f"Costs:{costs}")
print("Total:", total)
print(idx_total)
print("Charges:", charges, f"= {sum_charges}")
print(f"Taxrate: {tax_rate}%")
# print(f"Tax: {tax_charge}")
