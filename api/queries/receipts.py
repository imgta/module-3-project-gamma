from models.receipts import (
    Receipt,
    ReceiptOut,
    ReceiptCreate,
)
from queries.pool import pool
from typing import List


class ReceiptQueries:
    def create_receipt(self, receipt: ReceiptCreate) -> Receipt:
        with pool.connection() as conn, conn.cursor() as db:
            result = db.execute(
                """
                INSERT INTO receipts
                (venue_name, address, total_cost, image_url, transaction_date,
                assigned_group, settled, receipt_name)
                VALUES
                (%s, %s, %s, %s, %s, %s, %s, %s)
                RETURNING id;
                """,
                [
                    receipt.venue_name,
                    receipt.address,
                    receipt.total_cost,
                    receipt.image_url,
                    receipt.transaction_date,
                    receipt.assigned_group,
                    receipt.settled,
                    receipt.receipt_name,
                ],
            )
            id = result.fetchone()[0]
            return self.receipt_in_to_out(id, receipt)

    def get_all_receipts(self) -> List[ReceiptOut]:
        with pool.connection() as conn, conn.cursor() as db:
            result = db.execute(
                """
                SELECT id, venue_name, address, total_cost, image_url ,
                transaction_date, assigned_group, settled, receipt_name
                FROM receipts
                ORDER BY id;
                """
            )
            result = []
            for row in db:
                receipt = ReceiptOut(
                    id=row[0],
                    venue_name=row[1],
                    address=row[2],
                    total_cost=row[3],
                    image_url=row[4],
                    transaction_date=row[5],
                    assigned_group=row[6],
                    settled=row[7],
                    receipt_name=row[8],
                )
                result.append(receipt)
            return result

    def get_one(self, receipt_id) -> ReceiptOut:
        with pool.connection() as conn, conn.cursor() as db:
            result = db.execute(
                """
                SELECT id
                , venue_name
                , address
                , total_cost
                , image_url
                , transaction_date
                , assigned_group
                , settled
                , receipt_name
                FROM receipts
                WHERE id = %s
                """,
                [receipt_id],
            )
            record = result.fetchone()
            if record is None:
                return None
            return self.record_to_receipt_out(record)

    def update_receipt(self, receipt_id: int, receipt: Receipt) -> ReceiptOut:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                UPDATE receipts
                SET venue_name = %s
                , address = %s
                , total_cost = %s
                , image_url = %s
                , transaction_date = %s
                , assigned_group = %s
                , settled = %s
                , receipt_name = %s
                WHERE id = %s
                """,
                [
                    receipt.venue_name,
                    receipt.address,
                    receipt.total_cost,
                    receipt.image_url,
                    receipt.transaction_date,
                    receipt.assigned_group,
                    receipt.settled,
                    receipt.receipt_name,
                    receipt_id,
                ],
            )
            return self.receipt_in_to_out(receipt_id, receipt)

    def delete_receipt(self, receipt_id: int) -> None:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                DELETE FROM receipts
                WHERE id = %s
                """,
                [receipt_id],
            )
            return True

    def receipt_in_to_out(self, id: int, receipt: Receipt):
        old_data = receipt.dict()
        return ReceiptOut(id=id, **old_data)

    def record_to_receipt_out(self, record):
        return ReceiptOut(
            id=record[0],
            venue_name=record[1],
            address=record[2],
            total_cost=record[3],
            image_url=record[4],
            transaction_date=record[5],
            assigned_group=record[6],
            settled=record[7],
            receipt_name=record[8],
        )
