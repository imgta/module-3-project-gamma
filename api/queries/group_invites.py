from models.group_invites import (
    InviteStatus,
    InviteList,
    InvitesOut,
    Invites,
)
from queries.pool import pool


class GroupInviteQueries:
    def create_invite(
        self,
        invite: InvitesOut,
        status: InviteStatus = InviteStatus.INVITED,
    ) -> Invites:
        with pool.connection() as conn, conn.cursor() as db:
            result = db.execute(
                """
                INSERT INTO group_invites (group_id, invited_user, status)
                VALUES (%s, %s, %s)
                RETURNING id;
                """,
                [
                    invite.group_id,
                    invite.invited_user,
                    status,
                ],
            )
            id = result.fetchone()[0]
            return Invites(
                id=id,
                group_id=invite.group_id,
                invited_user=invite.invited_user,
                status=status,
            )

    def get_all_invites(self) -> InviteList:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                SELECT *
                FROM group_invites
                ORDER BY id;
                """
            )
            records = []
            for row in db.fetchall():
                records.append(
                    Invites(
                        id=row[0],
                        group_id=row[1],
                        invited_user=row[2],
                        status=row[3],
                    )
                )
            return records

    def get_invite_by_id(self, id: int) -> Invites:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                SELECT *
                FROM group_invites
                WHERE id = %s;
                """,
                [id],
            )
            record = db.fetchone()
            if record is None:
                return None
            return Invites(
                id=record[0],
                group_id=record[1],
                invited_user=record[2],
                status=record[3],
            )

    def invites_by_groupid(self, group_id) -> InviteList:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                SELECT *
                FROM group_invites
                WHERE group_id = %s;
                """,
                [group_id],
            )
            columns = [script[0] for script in db.description]
            invites = []
            for row in db.fetchall():
                invites.append(dict(zip(columns, row)))
            return invites

    def invites_for_user(self, invited_user) -> InviteList:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                SELECT *
                FROM group_invites
                WHERE invited_user = %s;
                """,
                [invited_user],
            )
            columns = [script[0] for script in db.description]
            invites = []
            for row in db.fetchall():
                invites.append(dict(zip(columns, row)))
            return invites
