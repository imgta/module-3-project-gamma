from models.items import ItemIn, ItemOut
from queries.pool import pool
from typing import List


class ItemQueries:
    def create(self, item: ItemIn) -> ItemOut:
        with pool.connection() as conn, conn.cursor() as db:
            result = db.execute(
                """
                INSERT INTO receipt_items
                (item_name, item_qty, item_cost, receipt_id)
                VALUES
                (%s, %s, %s, %s)
                RETURNING id
                """,
                [
                    item.item_name,
                    item.item_qty,
                    item.item_cost,
                    item.receipt_id,
                ],
            )

            id = result.fetchone()[0]
            return self.item_in_to_out(id, item)

    def get_all(self) -> List[ItemOut]:
        with pool.connection() as conn, conn.cursor() as db:
            result = db.execute(
                """
                SELECT id, item_name, item_qty, item_cost, receipt_id
                FROM receipt_items
                ORDER BY item_name;
                """
            )
            result = []
            for record in db:
                item = ItemOut(
                    id=record[0],
                    item_name=record[1],
                    item_qty=record[2],
                    item_cost=record[3],
                    receipt_id=record[4],
                )
                result.append(item)
            return result

    def get_one(self, item_id: int) -> ItemOut:
        with pool.connection() as conn, conn.cursor() as db:
            result = db.execute(
                """
                SELECT id
                    , item_name
                    , item_qty
                    , item_cost
                    , receipt_id
                FROM receipt_items
                WHERE id = %s
                """,
                [item_id],
            )
            record = result.fetchone()
            if record is None:
                return None
            return ItemOut(
                id=record[0],
                item_name=record[1],
                item_qty=record[2],
                item_cost=record[3],
                receipt_id=record[4],
            )

    def delete(self, item_name: str) -> None:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                DELETE from receipt_items
                WHERE id = %s
                """,
                [item_name],
            )
            return True

    def update(self, item_id: int, item: ItemIn) -> ItemOut:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                UPDATE receipt_items
                SET item_name = %s
                , item_qty = %s
                , item_cost = %s
                , receipt_id = %s
                WHERE id = %s
                """,
                [
                    item.item_name,
                    item.item_qty,
                    item.item_cost,
                    item.receipt_id,
                    item_id,
                ],
            )
            return self.item_in_to_out(item_id, item)

    def item_in_to_out(self, id: int, item: ItemIn):
        old_data = item.dict()
        return ItemOut(id=id, **old_data)
