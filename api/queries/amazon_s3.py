from models.receipts import ReceiptUpload, Receipt
from queries.pool import pool


class S3Queries:
    def upload_receipt(
        self, receipt: ReceiptUpload, image_url: str
    ) -> Receipt:
        with pool.connection() as conn, conn.cursor() as db:
            exec = db.execute(
                """
                INSERT INTO receipts (group_id, image_url)
                VALUES (%s, %s)
                RETURNING id;
                """,
                [receipt.group_id, image_url],
            )
            id = exec.fetchone()[0]
            return Receipt(
                id=id,
                venue=receipt.venue,
                address=receipt.address,
                total_cost=receipt.total_cost,
                image_url=receipt.image_url,
                transaction_date=receipt.transaction_date,
                group_id=receipt.group_id,
                settled=receipt.settled,
                receipt_name=receipt.receipt_name,
            )
