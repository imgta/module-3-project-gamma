from models.groups import Group, GroupList, GroupOut, GroupOutList, GroupCreate
from queries.pool import pool
from datetime import date
import secrets
import string
import uuid


class GroupQueries:
    @staticmethod
    def invite_str():
        chars = string.ascii_letters + string.digits
        token = "".join(secrets.choice(chars) for _ in range(6))
        uuid_chars = str(uuid.uuid4())[:4]
        uuid_mix = "".join(
            secrets.choice(uuid_chars) for _ in range(len(uuid_chars))
        )
        return token + uuid_mix

    def create_group(self, group: GroupCreate, creator: str) -> Group:
        with pool.connection() as conn, conn.cursor() as db:
            invite_token = self.invite_str()
            db.execute(
                """
                WITH new_group AS (
                    INSERT INTO groups (name, creator, description,
                    invite_link)
                    VALUES (%s, %s, %s, %s)
                    RETURNING id
                )
                INSERT INTO group_invites (group_id, invited_user, status)
                SELECT id, %s, %s FROM new_group
                RETURNING id;
                """,
                [
                    group.name,
                    creator,
                    group.description,
                    invite_token,
                    creator,
                    "ACCEPTED",
                ],
            )
            group_id = db.fetchone()[0]
            return Group(
                id=group_id,
                name=group.name,
                creator=creator,
                description=group.description,
                invite_link=invite_token,
                created_on=date.today(),
            )

    def get_all_groups(self) -> GroupList:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                SELECT *
                FROM groups
                ORDER BY id;
                """
            )
            records = []
            for row in db.fetchall():
                records.append(
                    GroupOut(
                        id=row[0],
                        name=row[1],
                        creator=row[2],
                        description=row[3],
                        # invite_link=row[4],
                        created_on=row[5],
                    )
                )
            return records

    def get_groups_for_username(self, username: str) -> GroupOutList:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                SELECT gi.group_id, g.name, gi.invited_user, gi.status
                FROM groups g
                INNER JOIN group_invites gi ON g.id = gi.group_id
                WHERE gi.invited_user = %s
                ORDER BY g.id;
                """,
                [username],
            )
            columns = [script[0] for script in db.description]
            groups = []
            for row in db.fetchall():
                groups.append(dict(zip(columns, row)))
            return groups

    def get_group_by_id(self, id: int) -> Group:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                SELECT *
                FROM groups
                WHERE id = %s;
                """,
                [id],
            )
            record = db.fetchone()
            if record is None:
                return None
            return Group(
                id=record[0],
                name=record[1],
                creator=record[2],
                description=record[3],
                invite_link=record[4],
                created_on=record[5],
            )

    def get_group_by_name(self, name) -> GroupOutList:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                SELECT *
                FROM groups
                WHERE name = %s;
                """,
                [name],
            )
            columns = [script[0] for script in db.description]
            groups = []
            for row in db.fetchall():
                groups.append(dict(zip(columns, row)))
            return groups

    def groups_by_creator(self, creator) -> GroupList:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                SELECT *
                FROM groups
                WHERE creator = %s;
                """,
                [creator],
            )
            columns = [script[0] for script in db.description]
            groups = []
            for row in db.fetchall():
                groups.append(dict(zip(columns, row)))
            return groups

    def delete_group(self, name: str) -> None:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                DELETE FROM groups
                WHERE name=%s;
                """,
                [name],
            )
