from queries.pool import pool
from models.users import (
    UserOutWithPassword,
    UserUpdate,
    UserOut,
    UserIn,
    User,
)


class UserQueries:
    def get_all_users(self):
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                SELECT id, fullname, email, username, hashed_password,
                birthdate
                FROM users
                ORDER BY id;
                """
            )
            records = []
            for row in db.fetchall():
                records.append(
                    UserOutWithPassword(
                        id=row[0],
                        fullname=row[1],
                        email=row[2],
                        username=row[3],
                        hashed_password=row[4],
                        birthdate=row[5],
                    )
                )
            return records

    def get_by_username(self, username: str) -> User:
        with pool.connection() as conn, conn.cursor() as db:
            result = db.execute(
                """
                SELECT id, fullname, email, username, hashed_password,
                birthdate
                FROM users
                WHERE username = %s;
                """,
                [username],
            )
            record = result.fetchone()
            if record is None:
                return None
            return User(
                id=record[0],
                fullname=record[1],
                email=record[2],
                username=record[3],
                hashed_password=record[4],
                birthdate=record[5],
            )

    def get_by_id(self, id: int) -> User:
        with pool.connection() as conn, conn.cursor() as db:
            result = db.execute(
                """
                SELECT id, fullname, email, username, hashed_password,
                birthdate
                FROM users
                WHERE id = %s;
                """,
                [id],
            )
            record = result.fetchone()
            if record is None:
                return None
            return User(
                id=record[0],
                fullname=record[1],
                email=record[2],
                username=record[3],
                hashed_password=record[4],
                birthdate=record[5],
            )

    # CREATE A NEW USER
    def user_create(self, user: UserIn, hashed_password: str) -> User:
        with pool.connection() as conn, conn.cursor() as db:
            result = db.execute(
                """
                INSERT INTO users (fullname, email, username, hashed_password,
                birthdate)
                VALUES (%s, %s, %s, %s, %s)
                RETURNING id;
                """,
                [
                    user.fullname,
                    user.email,
                    user.username,
                    hashed_password,
                    user.birthdate,
                ],
            )
            id = result.fetchone()[0]
            return User(
                id=id,
                fullname=user.fullname,
                email=user.email,
                username=user.username,
                hashed_password=hashed_password,
                birthdate=user.birthdate,
            )

    def user_update(self, username: str, user: UserUpdate) -> User:
        with pool.connection() as conn, conn.cursor() as db:
            db.execute(
                """
                UPDATE users
                SET fullname = %s, email = %s, hashed_password = %s
                WHERE username = %s
                RETURNING id, fullname, email, username, hashed_password,
                birthdate;
                """,
                [user.fullname, user.email, user.new_password, username],
            )
            record = db.fetchone()
            if record is None:
                return None
            return User(
                id=record[0],
                fullname=record[1],
                email=record[2],
                username=record[3],
                hashed_password=record[4],
                birthdate=record[5],
            )

    def user_in_to_user_out(self, id: int, user: UserIn):
        old_data = user.dict()
        return UserOut(id=id, **old_data)
