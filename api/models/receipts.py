from pydantic import BaseModel
from datetime import date
from typing import List


class Receipt(BaseModel):
    venue_name: str
    address: str
    total_cost: float
    image_url: str = ""
    transaction_date: date
    assigned_group: int
    settled: bool = False
    receipt_name: str = ""


class ReceiptOut(BaseModel):
    id: int
    venue_name: str
    address: str
    total_cost: float
    transaction_date: date
    assigned_group: int
    settled: bool
    receipt_name: str


class ReceiptList(BaseModel):
    __root__: List[Receipt]


class ReceiptCreate(BaseModel):
    venue_name: str
    address: str
    total_cost: float
    image_url: str = ""
    transaction_date: date
    assigned_group: int
    settled: bool = False
    receipt_name: str = ""


class ListReceiptCreate(BaseModel):
    __root__: List[ReceiptCreate]


class ReceiptUpload(BaseModel):
    assigned_group: int
    image_url: str


class ReceiptItem(BaseModel):
    receipt_id: int
    item_name: str
    item_qty: int = 1
    item_cost: float


class ItemExpense(BaseModel):
    item_id: int
    ordered_by: List[int]
    paid_by: List[int]
    amt_owed: float
    settled: bool
