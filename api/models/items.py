from pydantic import BaseModel


class ItemIn(BaseModel):
    item_name: str
    item_qty: int
    item_cost: float
    receipt_id: int


class ItemOut(BaseModel):
    id: int
    item_name: str
    item_qty: int
    item_cost: float
    receipt_id: int
