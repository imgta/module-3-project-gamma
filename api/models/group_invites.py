from pydantic import BaseModel
from typing import List, Dict
from enum import Enum


class InviteStatus(str, Enum):
    ACCEPTED = "ACCEPTED"
    INVITED = "INVITED"
    REJECTED = "REJECTED"


class Invites(BaseModel):
    id: int
    group_id: int
    invited_user: str
    status: InviteStatus


class InvitesOut(BaseModel):
    group_id: int
    invited_user: str


class InviteList(BaseModel):
    __root__: List[Invites]


class UserInviteList(BaseModel):
    __root__: List[str]


class UserGroupsList(BaseModel):
    __root__: Dict[str, List[Dict[str, str]]]


class InviteOutList(BaseModel):
    __root__: List[InvitesOut]
