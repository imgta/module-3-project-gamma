from jwtdown_fastapi.authentication import Token
from pydantic import BaseModel, EmailStr
from datetime import date
from typing import Optional


class DuplicateUserError(ValueError):
    pass


class User(BaseModel):
    id: int
    fullname: str
    email: EmailStr
    username: str
    hashed_password: str
    birthdate: date


class UserIn(BaseModel):
    fullname: str = "name"
    email: EmailStr = "user@email.com"
    username: str = "user"
    password: str = "pass"
    birthdate: date


class UserOut(BaseModel):
    id: int
    fullname: str
    email: EmailStr
    username: str
    birthdate: date


class UserUpdate(BaseModel):
    fullname: Optional[str]
    email: Optional[EmailStr]
    new_password: Optional[str]


class UserOutWithPassword(UserOut):
    hashed_password: str


class UserForm(BaseModel):
    username: str
    password: str


# TOKENS FROM HTTP-ONLY COOKIES
class UserToken(Token):
    user: UserOut


class HttpError(BaseModel):
    detail: str
