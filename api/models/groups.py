from pydantic import BaseModel
from datetime import date
from typing import List


class Group(BaseModel):
    id: int
    name: str
    creator: str
    description: str
    invite_link: str
    created_on: date


class GroupList(BaseModel):
    __root__: List[Group]


class GroupOut(BaseModel):
    id: int
    name: str
    creator: str
    description: str
    created_on: date


class GroupOutList(BaseModel):
    __root__: List[GroupOut]


class GroupCreate(BaseModel):
    name: str = "GROUP"
    description: str = "category"


class GroupToken(BaseModel):
    invite_link: str
